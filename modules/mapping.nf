#!/usr/bin/env nextflow

/*
 * Copyright (c) 2020, European Research Institute 
 * for the Biology of Ageing(ERIBA)<http://eriba.umcg.nl/>
 * Department of the aging biologyha
*/

/*
 * A proteogenomics data integration pipeline implemented in nextflow
 *
 * Authors:
 * - Stijn Arends <stijnarends@live.nl>
 *
 * Version:
 * - v1.0
 *
 * Date:
 * - 2-10-2020
*/

// Calculate the overhang that will be used in the buildIndex and STAR processes
sjdbOverhang = params.sequence_length - 1

/*
 The MAPPING workflow is apart of the workflows called by the main
 workflow that is located in the main.nf script.
 
 It takes the trimmed reads from the trimmomaticSE or trimmomaticPE
 process and maps those reads using STAR.
 
 Take:
	trimm_reads_ch: Channel containing the index, sample ID, sample specific output path, and trimmed reads
	
 Emit:
 	output files produced by STAR
*/
workflow MAPPING {
	take:
		trimm_reads_ch
		
	main:
		// If paired end reads are supllied run the StarPE process, else run the StarSE process
		def result_star = (params.end == "PE") ? StarPE(trimm_reads_ch, params.genomeIndex, params.annotation, params.references, params.genome) : StarSE(trimm_reads_ch, params.genomeIndex, params.annotation, params.references, params.genome)

	emit:
		count_table = result_star[0]
		aligned_reads = result_star[1]
		unmapped_reads = result_star[2]
		splice_juntion_tab = result_star[3]
		indexed_reads = result_star[4]
		unmapped_reads_2 = result_star[5]
}

/*---------*
* PROCESSES
*----------*/

/*
 Process StarPE mapps the reads to a reference genome using STAR.
 The aligned reads are then indexed with samtools.
	
 Input:
	tuple val(index), val(sample), path(sample_outdir), path(reads)
		Tuple containing the index, sample ID, sample output dir, and reads
		
 Output:
	counts - path
		Count table of the genes
	align - path
		Mapped reads in .bam format
	unmapped1 - path
		Unmapped reads in .fastq format
	indexed_reads - path
		Indexed aligned reads
	unmapped2 - path
		Unmapped reads in .fastq format
		
*/

process StarPE {
	label 'star_samtool'
	memory '40G'

	cpus 8

	input:
	tuple val(index), val(sample), path(output_dir), path(reads)
	path genome_index
	path annotation
	path reference_dir
	path genome
	
	output:
	path "${basename}_ReadsPerGene.out.tab", emit: "counts"
	tuple val(index), val(sample), path(output_dir), path("${bam}"), emit: "align"
	tuple val(index), path("${unmapped}.mate1"), emit: "unmapped1"
	tuple val(index), path("${basename}_SJ.out.tab"), emit: "splice_junctions"
	tuple val(index), val(sample), path(output_dir), path("${bam}.bai"), emit: 'indexed_reads'
	tuple val(index), val(sample), path(output_dir), path("${unmapped}.mate2"), emit: "unmapped2"
	
	script:
	ref_path = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/"
	output_path = "${output_dir}/StarPE/mapping/"
	basename = "${output_path}/${sample}"
	bam = "${basename}_Aligned.sortedByCoord.out.bam"
	unmapped = "${basename}_Unmapped.out"
	"""
	echo "Process: StarPE";
	echo "Index: '${ref_path}/${index}'";
	echo "Sample: '${ref_path}/${sample}'";
	echo "Output dir: '${output_path}'";
	echo "Reads: '${reads}'";
	echo "Annotation: '${ref_path}/genome/${annotation}'";
	echo "Fasta: '${ref_path}/genome/${params.species}_genome.fa'";

	mkdir -p "${output_path}";

	STAR \
		--runThreadN ${task.cpus} \
		--genomeDir "${ref_path}/index/" \
		--readFilesCommand zcat \
		--readFilesIn "${reads[0]}" "${reads[1]}" \
		--outSAMstrandField intronMotif \
		--sjdbGTFfile "${ref_path}/genome/${annotation}" \
		--sjdbOverhang ${sjdbOverhang} \
		--outSAMmapqUnique 50 \
		--outSAMtype BAM SortedByCoordinate \
		--outSAMattrRGline ID:${sample} SM:${sample} LB:${sample} PL:ILLUMINA \
		--quantMode GeneCounts \
		--outReadsUnmapped Fastx \
		--outFileNamePrefix "${basename}_" ;

	samtools index "${bam}";
	"""
}

/*
 Process StarSE mapps the single end reads to a reference genome using STAR.
 The aligned reads are then indexed with samtools.
	
 Input:
	tuple val(index), val(sample), path(sample_outdir), path(reads)
		Tuple containing the index, sample ID, sample output dir, and reads
		
 Output:
	counts - path
		Count table of the genes
	align - path
		Mapped reads in .bam format
	unmapped1 - path
		Unmapped reads in .fastq format
	splice_junctions - path
		Splice junction tables
	indexed_reads - path
		Indexed aligned reads		
*/

process StarSE {
	label 'star_samtool'
	cpus 4
	memory '40G'

	input:
	tuple val(index), val(sample), path(output_dir), path(reads)
	path genome_index
	path annotation
	path reference_dir
	path genome

	output:
	path "${basename}_ReadsPerGene.out.tab", emit: "counts"
	tuple val(index), val(sample), path(output_dir), path("${bam}"), emit: "align"
	tuple val(index), path("${unmapped}.mate1"), emit: "unmapped1"
	tuple val(index), path("${basename}_SJ.out.tab"), emit: 'splice_junctions'
	tuple val(index), val(sample), path(output_dir), path("${bam}.bai"), emit: 'indexed_reads'

	script:
	ref_path = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/"
	output_path = "${output_dir}/StarSE/mapping/"
	basename = "${output_path}/${sample}"
	bam = "${basename}_Aligned.sortedByCoord.out.bam"
	unmapped = "${basename}_Unmapped.out"
	"""
	echo "Process StarSE"
	echo "Index: '${ref_path}/${index}'"
	echo "Sample: '${ref_path}/${sample}'"
	echo "Output dir: ${output_path}"
	echo "Reads: ${reads}"

	mkdir -p "${output_path}";

	STAR \
		--runThreadN ${task.cpus} \
		--genomeDir "${ref_path}/index/" \
		--readFilesCommand zcat \
		--readFilesIn "${reads}" \
		--outSAMstrandField intronMotif \
		--outFileNamePrefix "${basename}_" \
		--sjdbGTFfile "${ref_path}/genome/${annotation}" \
		--sjdbOverhang ${sjdbOverhang} \
		--outSAMmapqUnique 50 \
		--outSAMtype BAM SortedByCoordinate \
		--outSAMattrRGline "ID:${sample} SM:${sample} LB:${sample} PL:${sample}" \
		--quantMode GeneCounts \
		--outReadsUnmapped Fastx ;

	samtools index "${bam}";
	"""
}
