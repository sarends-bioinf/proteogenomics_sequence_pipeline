#!/usr/bin/env nextflow

/*
 * Copyright (c) 2020, European Research Institute 
 * for the Biology of Ageing(ERIBA)<http://eriba.umcg.nl/>
 * Department of the aging biology
*/

/*
 * A proteogenomics data integration pipeline implemented in nextflow
 *
 * Authors:
 * - Stijn Arends <stijnarends@live.nl>
 *
 * Version:
 * - v1.0
 *
 * Date:
 * - 2-10-2020
*/

/*
 * Will include 3 sections with different processes:
	Variant calling:
 	- GATK
 	- qualityFilterVCF
 	- VEP
 	- processVEP

 	Transcript assembly (stringtie)
 	* two categories : known and novel(unknown transcripts)
 	- stringTie
 	- processStringtieOutput
 	- gffReadNovelSeqs
 	- gffReadExonSeqs
 	- spliceJunctions

 	De novo assembly  (trinity)
	- 
*/

workflow PROTEIN_DB {

	take:
		aligned_reads
		indexed_reads
		splice_junctions
		unmapped_reads1
		unmapped_reads2
		trimmed_unpaired

	main:
		// -Variant Calling--
		gatk(aligned_reads, params.genome, params.genome_dict, params.genome_fasta_index, params.references)

		// Filter the VCF file
		qualityFilterVcf(gatk.out.gatk_out)

		// Run VEP
		variantEffectPredictor(qualityFilterVcf.out.quality_filter_out, params.references, params.genome)

		// HAPLOSAURUS NOT IMPLMENTED YET!
		// filterPhasedVariants(qualityFilterVcf.out.quality_filter_out)
		// haplosaurus(filterPhasedVariants.out.phased, params.references, params.genome)

		// --Transcript Assembly--
		stringTie(aligned_reads, params.annotation, params.references)

		// Process stringTie output
		processStringtieOutput(stringTie.out.stringtie_out, params.references)

		// gffRead
		gffReadNovelSeqs(processStringtieOutput.out.novel, params.genome, params.references)
		gffReadExonSeqs(processStringtieOutput.out.exons, params.genome, params.references)

		// Splice junctions
		splice_junctions_in = gffReadExonSeqs.out.exon_seqs.join(splice_junctions, by: 0)
		spliceJunctions(splice_junctions_in)

		// Create input for proccesVEPOutput
		process_vep_in = processStringtieOutput.out.fpkmJSON.join(variantEffectPredictor.out.vep_out, by: 0)

		processVEPoutput(process_vep_in, params.references)

		// --TRINITY ASSAMBLY--
		if ( params.end == "PE" ) {
			// Paired end reads
			getDiscordant(aligned_reads)

			unpaired_discordant = trimmed_unpaired.join(getDiscordant.out.discordant_fq, by: 0)

			merge_suffix_left_in = unpaired_discordant.join(unmapped_reads1, by: 0)

			mergeAndSuffixLeftPE(merge_suffix_left_in)

			mergeAndSuffixRight(unmapped_reads2)

			// Trinity
			trinityPE_in = mergeAndSuffixLeftPE.out.suffixedfqPE.join(mergeAndSuffixRight.out.suffixedfqRight, by: 0)

			trinityPE(trinityPE_in)

			// Rename trinity files
			renameTrinityFiles(trinityPE.out.trinityPE)

			// Combine files
			trinity_gffnovel = renameTrinityFiles.out.renamed_trinity.join(gffReadNovelSeqs.out.novel_seqs, by: 0)
			concat_fasta_in = trinity_gffnovel.join(processVEPoutput.out.varNuclSeq, by: 0)

			concatFasta(concat_fasta_in)

			transdecoderLongestORFS(concatFasta.out.merged_fa)

			blast(transdecoderLongestORFS.out.longest_orfs, params.blast_db)

			Pfam(transdecoderLongestORFS.out.longest_orfs, params.pfam_db)

			// Prepare input for transdecoerPredict
			mergedFA_blastp = concatFasta.out.merged_fa.join(blast.out.blastp, by: 0)
			transdecoder_predict_in = mergedFA_blastp.join(Pfam.out.main, by: 0)

			// Causes error:
			// transdecoderPredict(transdecoder_predict_in)

		} else {
			// Single end reads
			println "Trinity assembbly not implemented for single end reads ATM! Need to fix some issues."
			// mergeAndSuffixLeftSE(unmapped_reads1)

			// trinitySE(mergeAndSuffixLeftSE.out.suffixedfqSE)

			// renameTrinityFiles(trinitySE.out.trinitySE)

		}
}


/*----------------*
 * VARIANT CALLING
 *----------------*/

/*
 Process gatk is used to identify SNPs and indels in germline DNA and 
 RNAseq data using gatk. It takes in a read that has been mapped using STAR.
 The gatk option HaplotypeCaller is used to perform variant calling on germline
 SNPs and indels via local re-assembly of haplotypes
	
 Input:
	aligned_read - path
		A file containing a .bam file that has been mapped
		
 Output:
	gatk_out - path
		FIle containing the variants in .vcf.gz format
*/ 
process gatk {
	label 'gatk'
	cpus 16

	input:
	tuple val(index), val(sample), path(output_dir), path(aligned_reads)
	path genome
	path genome_dict
	path fasta_index
	path reference_dir

	output:
	tuple val(index), val(sample), path(output_dir), path(output_file), emit:'gatk_out'

	script:
	output_file = "${output_dir}/gatk/${sample}_gatk.vcf.gz"
	bam_file = "${output_dir}/StarPE/mapping/${aligned_reads}"
	"""
	echo "Process: gatk"
	echo "Index: ${index}"
	echo "Sample ID: ${sample}"
	echo "Sample path: ${output_dir}"
	echo "Aligned reads: ${aligned_reads}"

	mkdir -p "${output_dir}/gatk/"

	gatk \
		--java-options "-Xmx8G" \
		HaplotypeCaller \
		-R "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${genome}" \
		-I "${bam_file}" \
		-O "${output_file}" \
		--native-pair-hmm-threads ${task.cpus};
	"""
}

/*
 Process qualityFilterVcf filters the VCF for a minimum variant quality value.
 Take in the zipped VCF file produced by the gatk process. That file is then
 unzipped using gunzip and used as a parameter for the filterVcfQual.py script
 that filters the VCF file for a minimum variant quality value.

 Input:
	gatk_zipped - file
		A zipped file containing the .vcf file produced by gatk

 Output:
 	filtered_vcf - path
 		A file in vcf format that has been filter for a minimum variant quality
 		value.
*/

process qualityFilterVcf {
	label 'python3'

	input:
	tuple val(index), val(sample), path(output_dir), path(gatk_zipped)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_dir}/gatk/${sample}_gatk_filtered.vcf"), emit: 'quality_filter_out'

	script:
	"""
	echo "Process: qualityFilterVcf"
	echo "Index: ${index}"
	echo "Sample ID: ${sample}"
	echo "Sample path: ${output_dir}"
	echo "GATK input: ${gatk_zipped}"

	mkdir -p ${output_dir}/gatk

	python3 ${workflow.projectDir}/scripts/filterVCFQual.py ${gatk_zipped} \
	${output_dir}/gatk/${sample}_gatk_filtered.vcf ${params.vcfQual}
	"""
}

/*
 Process filterPhasedVariants separates the phased and unphased genotypes from the VCF file produced by GATK. 
 The phased and unphased genotypes are written out to separate VCF files

 input:
	filtered_gatk - path
		The VCF file filtered based on quality produced by qualityFilterVcf

 output:
	phased - path
		VCF file containing the phased genotypes
	unphased - path
		VCF file containing the unphased genotypes
*/

process filterPhasedVariants {
	label 'python3'

	input:
	tuple val(index), val(sample), path(output_dir), path(filtered_gatk)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_basename}_phased_variants.vcf"), emit:'phased'
	tuple val(index), val(sample), path(output_dir), path("${output_basename}_unphased_variants.vcf"), emit:'unphased'

	script:
	output_basename = "${output_dir}/gatk/${sample}"
	"""
	python3 ${workflow.projectDir}/scripts/processPhasedVariants.py \
		"${filtered_gatk}" \
		"${output_basename}_phased_variants.vcf" \
		"${output_basename}_unphased_variants.vcf";
	"""
}

/*
 Process that runs the haplosaurus software. 
 Haplosaurus takes phased genotypes from a VCF file and constructs a pair of haplotype sequences for each overlapped transcript.

 Input:
 	phased_variants - path 
 		A VCF file containing the phased variants produced by filterPhasedVariants

 Output:
 	haplo_out - path
 		Output file produced by haplosaurus
*/
process haplosaurus {
	label 'vep'

	input:
	tuple val(index), val(sample), path(output_dir), path(phased_variants)
	path reference_dir
	path genome

	output:
	tuple val(index), path("${output_file}"), emit:'haplo_out'

	script:
	command = "haplo"
	vep_cache_dir = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/VEP/"
	fasta_path  = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${genome}"
	output_path = "${output_dir}/vep/"
	output_file = "${output_path}/${sample}_${command}.txt"
	"""
	echo "Process: haplosaurus"
	echo "Index: ${index}";
	echo "Sample ID: '${sample}'";
	echo "Sample path: '${output_path}'";
	echo "Vep input: '${phased_variants}'";
	echo "Fasta input: '${fasta_path}'";

	mkdir -p "${output_path}";

	grep --before-context=10000 --after-context=10 --max-count=1 -P "^#[^#]" "${phased_variants}" 1> "small_subset.vcf";
	#-i "${phased_variants}"

	${command} \
		-i "${phased_variants}" \
		--force_overwrite \
		--cache \
		--offline \
		--species "${params.species}" 	--dir_cache "${vep_cache_dir}" --fasta "${fasta_path}" \
		-o "${output_file}";
	"""
}


/*
 Process variantEffectPredctor runs the VEP software. VEP allows you to annotate variants 
 with the genes and regulatory features they hit and what effect they have on them

 website: https://www.ensembl.org/info/docs/tools/vep/index.html

 Input:
 	filtered_gatk - path
 		The VCF file filtered based on quality produced by qualityFilterVcf

 Output:
 	vep_out - path
 		14 column tab-delimited file
*/
process variantEffectPredictor {
	label 'vep'

	input:
	tuple val(index), val(sample), path(output_dir), path(filtered_gatk)
	//path cache_dir
	path reference_dir
	path genome

	output:
	tuple val(index), path("${output_file}"), emit:'vep_out'

	script:
	command = "vep"
	vep_cache_dir = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/VEP/"
	fasta_path  = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${genome}"
	output_path = "${output_dir}/vep/"
	output_file = "${output_path}/${sample}_${command}.txt"
	"""
	echo "Index: ${index}";
	echo "Sample ID: '${sample}'";
	echo "Sample path: '${output_path}'";
	echo "Vep input: '${filtered_gatk}'";
	echo "Fasta input: '${fasta_path}'";

	mkdir -p "${output_path}";

	${command} \
		-i "${filtered_gatk}" \
		--force_overwrite \
		--cache \
		--offline \
		--species "${params.species}" \
		--dir_cache "${vep_cache_dir}" \
		--fasta "${fasta_path}" \
		-o "${output_file}";
	"""
} 

/*
 Processes the VEP predictions. 

 Input:
 	fpkm_json - path
 		FPKM values for each feature in json format produced by processStringTieOutput
 	vep - path
 		14 column tab-delimited file produced by VariantEffectPredictor

 Output:
 	varNuclSeq - path
		-
 	varPepSeq - path
 		-

*/
process processVEPoutput {
	label 'python3'

	input:
	tuple val(index), val(sample), path(output_dir), path(fpkm_json), path(vep)
	path reference_dir

	output:
	tuple val(index), path("${basename}_varNuclSeq.fa"), emit:'varNuclSeq'
	tuple val(index), path("${basename}_varPepSeq.fa"), emit:'VarPepSeq'

	script:
	genome_ref_basename = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${params.species}"
	extra_args = params.seq_type == "RNA" ? """ --FPKM_cutoff ${params.fpkm} --fpkmJson_file "${fpkm_json}" """ : " "
	basename = "${output_dir}/vep/${sample}"
	"""
		echo "Process: processVEPoutput"
		echo "Index: ${index}"
		echo "Sample ID: '${sample}'"
		echo "Sample path: '${output_dir}'"
		echo "FPKM json input: '${fpkm_json}'"
		echo "VEP file: '${vep}'"

		echo "Selected <${params.seq_type}>"

		python3 ${workflow.projectDir}/scripts/processVEPoutput.py \
				"${params.seq_type}" \
				"${genome_ref_basename}_cDNA.fa" \
				"${genome_ref_basename}_pep.fa" \
				"${vep}" \
				"${basename}_varPepSeq.fa" \
				"${basename}_varNuclSeq.fa" \
				${extra_args};
		"""
}


/*--------------------*
 * TRANSCRIPT ASSEMBLY
 *--------------------*/

/*
 Process getDiscordant reads extracts the discordant reads of aligned reads
 with samtools. These discordant reads are fed into Trinity.

 website: http://www.htslib.org/ 

 Input:
 	aligned_reads - path
 		The aligned reads produced by STAR

 Output:
 	discordant_sam - path
 		Discordant reads in sam format
 	discordant_fq - path
 		Discordant reads in fastq format 
*/
 process getDiscordant {
 	label 'star_samtool'

 	input:
 	tuple val(index), val(sample), path(output_dir), path(aligned_reads)

 	output:
 	tuple val(index), val(sample), path(output_dir), path("${output_dir}/discordant/${sample}_discordant.sam"), emit: 'discordant_sam'
 	// val(sample), path(output_dir). Not needed because of trimmed_unpaired from trimmomaticPE contains sample and output path
 	tuple val(index), path("${output_dir}/discordant/${sample}_discordant.fq"), emit: 'discordant_fq'

 	script:
 	"""
	echo "Index: ${index}"
	echo "Sample ID: ${sample}"
	echo "Sample path: ${output_dir}"
	echo "Discordant input: ${aligned_reads}"

 	mkdir -p ${output_dir}/discordant
 	samtools view -S -F 2 -h ${aligned_reads} -o ${output_dir}/discordant/${sample}_discordant.sam;
 	samtools fastq ${output_dir}/discordant/${sample}_discordant.sam > ${output_dir}/discordant/${sample}_discordant.fq;
 	"""
 }

 /*
  Process stringTie is used to assemble possible transcripts from RNA-seq data.

  website software: http://ccb.jhu.edu/software/stringtie/

  Input:
  	aligned_reads - path
  		The aligned reads produced by STAR
  	annotation - path
  		Gene annotation file
  	reference_dir - path
  		Path to the directory were all reference related files are stored

  Output:
  	stringtie_out - path
  		a Gene Transfer Format (GTF) file that contains details of the transcripts that StringTie assembles from RNA-Seq data
 */
 process stringTie {
 	label 'stringtie'
 	cpus 2

 	input:
 	tuple val(index), val(sample), path(output_dir), path(aligned_reads)
 	path annotation
 	path reference_dir

 	output:
 	tuple val(index), val(sample), path(output_dir), path(output_gtf), emit:'stringtie_out'

 	script:
 	annotation_path = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${annotation}"
	output_path = "${output_dir}/stringTie/"
	output_geneAbundance = "${output_path}/${sample}_geneAbundance.tab" // not exported??
	output_gtf = "${output_path}/${sample}_stringTie.gtf"
 	"""
 	echo "Process: stringTie"
	echo "Index: ${index}"
	echo "Sample ID: '${sample}'"
	echo "Sample path: '${output_path}'"
	echo "Vep input: '${aligned_reads}'"

 	mkdir -p "${output_path}";

 	stringtie "${aligned_reads}" \
 		-G "${annotation_path}" \
 		-A "${output_geneAbundance}" \
 		-o "${output_gtf}" \
 		-p ${task.cpus};
 	"""
 }

/*
 ProcessStringTieOutput processes the output produced by StringTie.
 The assembled transcripts in the produced GTF file are separated 
 into known and novel transcripts annotations for further processing. The annotation of the
 exons are extracted and the Fragments Per Kilobase of transcript per Million mapped
 reads(FPKM) are collected.

 Input:
 	stringtie_gtf - path
 		GTF file that containing details of the transcripts produced by StringTie
 	reference_dir - path
 		Path to the directory were all reference related files are stored

 Output:
 	known - path
 		Known transcript annotations in .fa format
 	novel - path
 		Novel transcript annotations in .gtf format
 	fpkmJSON - path
 		FPKM values in .json format
 	exons - path
 		Annotations of the exons in .gtf format
*/
process processStringtieOutput {
	label 'python3'

	input:
	tuple val(index), val(sample), path(output_dir), path(stringtie_gtf)
	path reference_dir

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_basename}_stringTie_known.fa"), emit:'known'
	tuple val(index), val(sample), path(output_dir), path("${output_basename}_stringTie_novel.gtf"), emit:'novel'
	tuple val(index), val(sample), path(output_dir), path("${output_basename}_fpkm.json"), emit:'fpkmJSON'
	tuple val(index), val(sample), path(output_dir), path("${output_basename}_exons.gtf"), emit:'exons'

	script:
	output_path = "${output_dir}/stringTie/"
	output_basename = "${output_path}/${sample}"
	genome_path ="${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/"
	"""
	echo "Process: processStringtieOutput";
	echo "Index: ${index}";
	echo "Sample ID: '${sample}'";
	echo "Sample path: '${output_dir}'";
	echo "StringTie output: '${stringtie_gtf}'";
	echo "StringTie output: '${stringtie_gtf}'";


	#mkdir -p "${output_path}";

	python3 "${workflow.projectDir}/scripts/processStringTieOutput.py" \
		"${genome_path}/${params.species}_pep.fa" \
		"${stringtie_gtf}" \
		"${output_basename}_stringTie_known.fa" \
		"${output_basename}_stringTie_novel.gtf" \
		"${output_basename}_fpkm.json" \
		"${output_basename}_exons.gtf" \
		"${params.fpkm}";
	"""
}

/*
 gffReadNovelSeqs transforms the novel sequences into fasta format with GFFREAD.

 website GFFREAD: http://ccb.jhu.edu/software/stringtie/gff.shtml#gffread

 Input:
 	stringtie_novel - path
 		Novel transcript annotations in .gtf format produced by processStringTieOutput
 	genome - path
 		Genome reference file
 	reference_dit -path
 		Path to the directory were all reference related files are stored

 Output:
 	novel_seqs - path
 		The novel transcript sequences in fasta format.
*/
process gffReadNovelSeqs {
	label 'gffread'

	input:
	tuple val(index), val(sample), path(output_dir), path(stringtie_novel)
	path genome
	path reference_dir

	output:
	// Not needed: val(sample), path(output_dir), because joining trinity tuple
	tuple val(index), path(output_file), emit: 'novel_seqs'

	script:
	genome_fasta = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${genome}"
	output_path = "${output_dir}/gffread/"
	output_file = "${output_path}/${sample}_novel_gffRead.fa"
	"""
	echo "Process: gffReadNovelSeqs";
	echo "Index: ${index}";
	echo "Sample ID: '${sample}'";
	echo "Sample path: '${output_path}'";
	echo "Novel gff input: '${stringtie_novel}'";

	mkdir -p "${output_path}/";

	gffread -w "${output_file}" -g "${genome_fasta}" "${stringtie_novel}";
	"""
}

/*
 gffReadExonSeqs transforms the exon sequences into fasta format with GFFREAD.

 website GFFREAD: http://ccb.jhu.edu/software/stringtie/gff.shtml#gffread

 Input:
 	stringtie_exons - path
 		Exon transcript annotations in .gtf format produced by processStringTieOutput
 	genome - path
 		Genome reference file
 	reference_dit -path
 		Path to the directory were all reference related files are stored

 Output:
 	exon_seqs - path
 		The exon sequences in fasta format.
*/
process gffReadExonSeqs {
	label 'gffread'

	input:
	tuple val(index), val(sample), path(output_dir), path(stringtie_exons)
	path genome
	path reference_dir

	output:
	tuple val(index), val(sample), path(output_dir), path(output_file), emit: 'exon_seqs'

	script:
	genome_fasta = "${reference_dir}/${params.species}/${params.genome_build}/${params.release}/genome/${genome}"
	output_path = "${output_dir}/gffread/"
	output_file = "${output_path}/${sample}_exons_gffRead.fa"
	"""
	echo "Process: gffReadExonSeqs";
	echo "Index: ${index}";
	echo "Sample ID: '${sample}'";
	echo "Sample path: '${output_path}'";
	echo "Gff exons input: '${stringtie_exons}'";

	mkdir -p "${output_path}";

	gffread -w "${output_file}" -g "${genome_fasta}" "${stringtie_exons}";
	"""
}

/*
 Process spliceJunctions combines the splice junction file produced by STAR
 and the exon file output from GFFRead to include alternative splice site of annotated transcripts.

 Input:
 	exon_seqs - path
 		The exon sequences in fasta format produced by gffReadExonSeqs
 	splice_junc - path
 		Splice junction tables produced by STAR

 Output:
 	spliced_out - path
 		Included alternative splie sites of annotated transcripts
*/
process spliceJunctions {

	label 'python3'

	input:
	tuple val(index), val(sample), path(output_dir), path(exon_seqs), path(splice_junc)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_dir}/splicing/${sample}_splicedExons.fa"), emit: 'spliced_out'

	script:
	"""
	echo "Process: spliceJunctions"
	echo "Index: ${index}"
	echo "Sample ID: ${sample}"
	echo "Sample path: ${output_dir}"
	echo "Splice junctions input: ${splice_junc}"

	mkdir -p ${output_dir}/splicing

	python3 ${workflow.projectDir}/scripts/spliceJunctions.py ${exon_seqs} ${splice_junc} \
	${output_dir}/splicing/${sample}_splicedExons.fa
	"""
}


/*------------------------------------------------*
 * TRINITY ASSEMBLY, QUANTIFICATION AND FPKM FILTER
 *------------------------------------------------*/


/*
 Process mergeAndSufficLeftSE transforms the unmapped reads into .fq format if single end reads are provided.
 Then adds a /1 suffix to the read IDs.

 Input:
	unmapped_reads1 - path
		Unmapped reads produced by STAR
	output_dir - path
		Output path for the results

 Output:
 	LeftTrinInSE - path
 		Left trinity input for single end reads in fq format
 	suffixedfqSE - path
 		Left trinity input for single end reads with /1 suffix added
*/
process mergeAndSuffixLeftSE {

	label 'python3'

	input:
	path unmapped_reads1
	path output_dir

	output:
	path "${output_dir}/results/trinity/*leftTrinIn.fq", emit: 'LeftTrinInSE'
	path "${output_dir}/results/trinity/*leftTrinIn.fq.suffixed.fq", emit: 'suffixedfqSE'

	script:
	def (full_file, sample_id) = (unmapped_reads1 =~ /(^[^_]*_[^_]).*/)[0]
	"""
	mkdir -p ${output_dir}/results/trinity

	cat ${unmapped_reads1} > ${output_dir}/results/trinity/${sample_id}_leftTrinIn.fq

	python3 ${workflow.projectDir}/scripts/readIDsuffix.py ${output_dir}/results/trinity/${sample_id}_leftTrinIn.fq \
	/1 --no_check
	"""
}

/*
 Process mergeAndSufficLeftPE transforms the unpaireed, unmapped, and
 discordant reads into .fq format if paired end reads are provided.
 Then adds a /1 suffix to the read IDs.

 Input:
	unmapped_reads1 - path
		Unmapped reads produced by STAR

 Output:
 	LeftTrinInPE - path
 		Left trinity input for paired end reads in fq format
 	suffixedfqPE - path
 		Left trinity input for paired end reads with /1 suffix added
*/
 process mergeAndSuffixLeftPE {

 	label 'python3'

 	input:
 	tuple val(index), val(sample), path(output_dir), path(unpaired_reads), path(discordant_reads), path(unmapped_reads)

  	output:
 	path "${output_dir}/trinity/${sample}_leftTrinIn.fq", emit: 'leftTrinInPE'
 	tuple val(index), val(sample), path(output_dir), path("${output_dir}/trinity/${sample}_leftTrinIn.fq.suffixed.fq"), emit: 'suffixedfqPE'

 	script:
 	"""
 	echo "Process mergeAndSuffixLeftPE"
	echo "Index: ${index}"
	echo "Sample ID: ${sample}"
	echo "Sample path: ${output_dir}"
	echo "Discordant reads: ${discordant_reads}"
	echo "MergeAndPE unpaired reads: ${unpaired_reads}"
	echo "Unmapped reads mergeAndPE: ${unmapped_reads}"

 	mkdir -p ${output_dir}/trinity
 	< ${unpaired_reads[0]} zcat > ${output_dir}/trimmomaticPE/${sample}_trimm_forward_unpaired.fastq
 	< ${unpaired_reads[1]} zcat > ${output_dir}/trimmomaticPE/${sample}_trimm_reverse_unpaired.fastq

 	cat ${output_dir}/trimmomaticPE/${sample}_trimm_forward_unpaired.fastq ${output_dir}/trimmomaticPE/${sample}_trimm_reverse_unpaired.fastq \
 	${unmapped_reads} ${discordant_reads} > ${output_dir}/trinity/${sample}_leftTrinIn.fq

 	python3 ${workflow.projectDir}/scripts/readIDsuffix.py ${output_dir}/trinity/${sample}_leftTrinIn.fq \
 	/1 --no_check
 	"""
 }

/*
 Process mergeAndSufficRight transforms the second set of unmapped reads
 produced by STAR into .fq format.
 Then adds a /2 suffix to the read IDs.

 Input:
	unmapped_reads1 - path
		Unmapped reads produced by STAR

 Output:
 	rightTrinIn - path
 		Right trinity input in fq format
 	suffixedfqRight - path
 		Right trinity input with /2 suffix added
*/
 process mergeAndSuffixRight {

 	label 'python3'

 	input:
 	tuple val(index), val(sample), path(output_dir), path(unmapped_reads2)

 	output:
 	path "${output_dir}/trinity/${sample}_rightTrinIn.fq", emit: 'rightTrinIn'
 	tuple val(index), path("${output_dir}/trinity/${sample}_rightTrinIn.fq.suffixed.fq"), emit:'suffixedfqRight'

 	script:
 	"""
 	echo "Process: mergeAndSuffixRight"
	echo "Index: ${index}"
	echo "Sample ID: ${sample}"
	echo "Sample path: ${output_dir}"
	echo "MergeAndSuffix right unmapped: ${unmapped_reads2}"

 	mkdir -p ${output_dir}/trinity

 	cat ${unmapped_reads2} > ${output_dir}/trinity/${sample}_rightTrinIn.fq
 	python3 ${workflow.projectDir}/scripts/readIDsuffix.py ${output_dir}/trinity/${sample}_rightTrinIn.fq \
 	/2 --no_check
 	"""
 }


/*
 Process trinitySE runs trinity when single end reads are provided.

 Input:
 	leftTrinInput - path
 		The left trinity input produced by mergeAndSuffixLeftSe

 Output:
 	trinitySE - path
 		Reconstructed transcriptomes in .fasta format
*/
process trinitySE {

	label 'trinity'

	input:
	path leftTrinInput
	path output_dir

	output:
	path "${output_dir}/results/trinity/*_trinity/Trinity.fasta", emit: 'trinitySE'

	script:
	def (full_file, sample_id) = (leftTrinInput =~ /(^[^_]*_[^_]).*/)[0]
	"""
	mkdir -p ${output_dir}/results/trinity

	Trinity --seqType fq --max_memory 8G --single ${leftTrinInput} --CPU ${task.cpus} \
	--output ${output_dir}/results/trinity/${sample_id}_trinity/
	"""
}

/*
 Process trinityPE runs trinity when paired end reads are provided.

 Input:
 	leftTrinInput - path
 		The left trinity input produced by mergeAndSuffixLeftPE
 	rightTrinInput - path
 		Right trinity input produced by mergeAndSuffixRight

 Output:
 	trinitySE - path
 		Reconstructed transcriptomes in .fasta format
*/
process trinityPE {

	label 'trinity'

	input:
	tuple val(index), val(sample), path(output_dir), path(leftTrinInput), path(rightTrinInput)

	output:
	tuple val(index), val(sample), path(output_dir) ,path("${output_dir}/trinity/${sample}_trinity/"), path("${output_dir}/trinity/${sample}_trinity/Trinity.fasta"), emit: 'trinityPE'

	script:
	"""
	echo "Process: trinityPE"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Output path: ${output_dir}"
	echo "Left Trin input: ${leftTrinInput}"
	echo "Right Trin input: ${rightTrinInput}"

	mkdir -p ${output_dir}/trinity

	Trinity --seqType fq --max_memory 8G --left ${leftTrinInput} --right ${rightTrinInput} \
	--output ${output_dir}/trinity/${sample}_trinity/
	"""
}


/*
 Renames the output of trinity to include the sampleID in the name.

 Input:
 	trin - path
 		Output of trinity

 Output:
 	renamed_trinity - path
 		Renamed trinity output to include sample ID
*/
process renameTrinityFiles {

	input:
	tuple val(index), val(sample), path(output_dir), path(output_dir_trin), path(trin)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_dir_trin}/${sample}_trinity.fasta"), emit:'renamed_trinity'

	script:
	"""
	echo "Process: renameTrinityFiles"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Input file: ${trin}"
	echo "Normal output dir: ${output_dir}"
	echo "Output dir trinity: ${output_dir_trin}"

	mv ${trin} ${output_dir_trin}/${sample}_trinity.fasta
	"""

}


/*-------------------------------------*
 * TRANSDECODER & ORF HOMOLOGY SEARCHES
 *-------------------------------------*/

/*
 Concatenate the nucleotide sequences produced from the processVEPOutput.py
 script, the novel annotations produced by GFFRead and the assembled transcripts produced by
 trinity
	
 Input:
 	trin_in - path
 		Assembled transcripts produced by trinity
 	gff_in - path
 		Novel annotations produced by GffReadNovelSeqs
 	vep_in - path
 		Nucleotide sequences produced by processVEPoutput

 Output:
 	merged_fa - path
 		Combined fasta file of the three input files
*/
process concatFasta {

	input:
	tuple val(index), val(sample), path(output_dir), path(trin_in), path(gff_in), path(vep_in)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_dir}/transdecoder/${sample}_merged.fa"), emit:'merged_fa'

	script:
	"""
	echo "Process: concatFasta"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Output dir: ${output_dir}"
	echo "Trin in: ${trin_in}"
	echo "Gff in: ${gff_in}"
	echo "VEP in: ${vep_in}"

	mkdir -p ${output_dir}/transdecoder

	cat ${trin_in} ${gff_in} ${vep_in} > ${output_dir}/transdecoder/${sample}_merged.fa
	"""
}

/*
 Process runs the Longest.Orfs functionalty of the transdecoder software. 

 Input:
 	merged_fa - path
 		Combined fasta file of the trinity output, novel seqs from gffreadm and processVEPoutput output

 Output:
 	longest_orfs - path
 		all ORFs meeting the minimum length criteria, regardless of coding potential
*/
process transdecoderLongestORFS {

	label 'transdecoder'

	input:
	tuple val(index), val(sample), path(output_dir), path(merged_fa)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_dir}/transdecoder/${sample}_merged.fa.transdecoder_dir"), path("${output_dir}/transdecoder/${sample}_merged.fa.transdecoder_dir/longest_orfs.pep"), emit: 'longest_orfs'

	script:
	"""
	echo "Process: transdecoderLongestORFS"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Output dir: ${output_dir}"
	echo "merged fasta file: ${merged_fa}"

	mkdir -p ${output_dir}/transdecoder
	cd ${output_dir}/transdecoder

	TransDecoder.LongOrfs -t ${merged_fa}
	"""
}

/*
 Run blastp

 Input:
  	transdecoder_dir - path
 		Location of the directory produced by transdecoder.Longest.Orfs
 	longest_orfs - path
 		all ORFs meeting the minimum length criteria, regardless of coding potential

 Output:
	blastp - path
		homology search using blastp
*/
process blast {
	label 'blast'

	input:
	tuple val(index), val(sample), path(output_dir), path(transdecoder_dir), path(longest_orfs)
	path blast_db

	output:
	tuple val(index), path(transdecoder_dir), path("${transdecoder_dir}/blast.outfmt6"), emit:'blastp'
	// val(sample), path(output_dir),
	// path "${transdecoder_dir}/blast.outfmt6", emit:'blastp'

	script:
	"""
	echo "Process: blast"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Outpputdir: ${output_dir}"
	echo "Transdecoder dir: ${transdecoder_dir}"
	echo "Longest orfs: ${longest_orfs}"

	blastp -query ${longest_orfs} -db ${blast_db}/${params.species}_db -max_target_seqs 1 -outfmt 6 -evalue 1e-5 -num_threads ${task.cpus} > \
	${transdecoder_dir}/blast.outfmt6
	"""
}
/*
 Run hmmscan on pfam database

 Input:
   	transdecoder_dir - path
 		Location of the directory produced by transdecoder.Longest.Orfs
 	longest_orfs - path
 		all ORFs meeting the minimum length criteria, regardless of coding potential

 Output:
 	main - path
 		tabular (space-delimited) file summarizing the per-domain output, 
 		with one data line per homologous domain detected in a query sequence for each homologous model

*/
process Pfam {
	label 'hmmer'

	input:
	tuple val(index), val(sample), path(output_dir), path(transdecoder_dir), path(longest_orfs)
	path pfam_db

	output:
	tuple val(index), path("${transdecoder_dir}/pfam.domtblout"), emit:'main'
	tuple val(index), path("${transdecoder_dir}/stats_log_stdout.txt"), emit:'stdout'

	script:
	"""
	echo "Process: blast"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Outputdir: ${output_dir}"
	echo "Transdecoder dir: ${transdecoder_dir}"
	echo "Longest orfs: ${longest_orfs}"
	echo "Pfam Database: ${pfam_db}"

	hmmscan --cpu ${task.cpus} --domtblout ${transdecoder_dir}/pfam.domtblout \
	${pfam_db}/Pfam-A.hmm ${longest_orfs} > ${transdecoder_dir}/stats_log_stdout.txt
	"""
}

/*
 Process runs the Predict functionalty of the transdecoder software.

 Input:
 	merged_fa - path
 		Combined fasta file of the trinity output, novel seqs from gffreadm and processVEPoutput output
 	transdecoder_dir - path
 		Location of the directory produced by transdecoder.Longest.Orfs
 	blast - path
 		homology search results using blastp
 	pfam - path
 		tabular (space-delimited) file summarizing the per-domain output, 
 		with one data line per homologous domain detected in a query sequence for each homologous model

*/
process transdecoderPredict  {
	label 'transdecoder'

	input:
	tuple val(index), val(sample), path(output_dir), path(merged_fa), path(transdecoder_dir), path(blast), path(pfam)

	output:
	tuple val(index), val(sample), path(output_dir), path("${output_dir}/transdecoder/${sample}_merged.fa.transdecoder.pep"), emit:'trans_predict'

	script:
	"""
	echo "Process: transdecoderPredict"
	echo "Index: ${index}"
	echo "Sample: ${sample}"
	echo "Outputdir: ${output_dir}"
	echo "Transdecoder dir: ${transdecoder_dir}"
	echo "Merged fa: ${merged_fa}"
	echo "Blast in: ${blast}"
	echo "Pfam in: ${pfam}"

	mkdir -p ${output_dir}/transdecoder
	cd ${transdecoder_dir}
	ls
	echo "--------SEPERATOR---------"
	cd ../
	ls 
	TransDecoder.Predict -t ${merged_fa} --retain_pfam_hits ${transdecoder_dir}/${pfam} \
	--retain_blastp_hits ${transdecoder_dir}/${blast} --no_refine_starts
	"""
}

