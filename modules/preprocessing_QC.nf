#!/usr/bin/env nextflow

/*
 The PREPROCESSING workflow is apart of the workflows called by the main
 workflow that is located in the main.nf script.
 
 It takes single end and or paired end reads and perform preprocessing and
 quality control on those reads using the fastQC and trimmomatic software.
 
 Take:
	samples_files: Channel containing the input reads plus their sample IDs
  
 Emit:
	Outputs the results of the trimmomatic step.
*/
workflow PREPROCESSING {
	take:
		samples_files
		
	main:
	 	// If paired end reads are supllied run the fastQC_PE process, else run the fastQC_SE process
		def result_fastqc = (params.end == "PE") ? fastQC_PE(samples_files, params.outdir) : fastQC_SE(samples_files, params.outdir)

		// If paired end reads are supllied run the trimmomaticPE process, else run the trimmomaticSE process
		def result_trimmomatic = (params.end == "PE") ? trimmomaticPE(samples_files, params.outdir) : trimmomaticSE(samples_files, params.outdir)

	emit:
		trimm_log = result_trimmomatic[0]
		trimm_reads = result_trimmomatic[1]
		trimm_unpaired_reads = result_trimmomatic[2]
		
}


/*---------*
* PROCESSES
*----------*/

/*
 Process fastQC_SE takes in .fastq files containing single end reads and performs quality control
 on these reads using FastQC.
 
 Input:
	tuple val(index), val(sample), path(read)
		Tuple containing the index, sample ID and reads
		
 Output:
	fastqc_zip - path
		A zip file containing the results of a sample
	fastqc_html - path
		A html file which contain the plots and other results
		of the fastQC analysis
	sample_path - path
		Output path for specif sample
*/
process fastQC_SE {
	label 'fastqc'

	input:
	tuple val(index), val(sample), path(read)
	path output_dir
	
	output:
	path "${output_dir}/proteogenomics_pipeline/${sample}/fastQC_SE/*.zip", emit: 'fastqc_zip'
	path "${output_dir}/proteogenomics_pipeline/${sample}/fastQC_SE/*.html", emit: 'fastqc_html'
	path "${output_dir}/proteogenomics_pipeline/${sample}", emit:'sample_path'
	
	script:
	"""
	mkdir -p ${output_dir}/proteogenomics_pipeline/${sample}/fastQC_SE

	fastqc -f fastq -q ${read} -o ${output_dir}/proteogenomics_pipeline/${sample}/fastQC_SE
	"""
}

/*
 Process fastQC_PE takes in .fastq files containing paired end reads and performs quality control
 on these reads using FastQC.
 
 Input:
	tuple val(index), val(sample), path(read_forward), path(read_reverse)
		Tuple containing the index, sample ID, forward and reverse reads
		
 Output:
	fastqc_zip - path
		A zip file containing the results of a sample
	fastqc_html - path
		A html file which contain the plots and other results
		of the fastQC analysis
	sample_path - path
		Output path for specif sample
*/
process fastQC_PE {
	label 'fastqc'

	input:
	tuple val(index), val(sample), path(read_forward), path(read_reverse)
	path output_dir

	output:
	path "${output_dir}/proteogenomics_pipeline/${sample}/fastQC_PE/*.zip", emit: 'fastqc_zip'
	path "${output_dir}/proteogenomics_pipeline/${sample}/fastQC_PE/*.html", emit: 'fastqc_html'
	path "${output_dir}/proteogenomics_pipeline/${sample}", emit:'sample_path'
	
	script:
	"""
	mkdir -p ${output_dir}/proteogenomics_pipeline/${sample}/fastQC_PE

	fastqc -f fastq -q ${read_forward} ${read_reverse} -o ${output_dir}/proteogenomics_pipeline/${sample}/fastQC_PE
	"""
}

/*
 Process trimmomaticPE performence preprocessing of the .fastq files 
 using the trimmomatic software of paired end reads.
 
 Input:
	tuple val(index), val(sample), path(read_forward), path(read_reverse)
		Tuple containing the index, sample ID, forward and reverse reads
		
 Output:
 	trimm_log - path
 		A log file of the trimmomatic process
	paired_trimmed_reads - tuple val(index), val(sample), path(sample_outdir), path(reads_paired)
		Tuple containing the index, sample ID, sample specific output path and forward and reverse paired reads
	unpaired_trimmed_reads - tuple val(index), val(sample), path(sample_outdir), path(reads_unpaired)
		Tuple containing the index, sample ID, sample specific output path and forward and reverse unpaired reads
*/
process trimmomaticPE {
	cpus 2
	label 'trimmomatic'

	input:
	tuple val(index), val(sample), path(read_forward), path(read_reverse)
	path output_dir
	
	output:
	path "${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/log/*.trimlog", emit: 'trimm_log'
	tuple val(index), val(sample), path("${output_dir}/proteogenomics_pipeline/${sample}/"), path("${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/*{forward,reverse}_paired.fastq.gz"), emit: 'paired_trimmed_reads'
	tuple val(index), val(sample), path("${output_dir}/proteogenomics_pipeline/${sample}/"), path("${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/*{forward,reverse}_unpaired.fastq.gz"), emit: 'unpaired_trimmed_reads'
	
	script:
	"""
	mkdir -p ${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/log

	java -jar /usr/src/Trimmomatic/0.38/Trimmomatic-0.38/trimmomatic-0.38.jar PE \
	-threads ${task.cpus} -trimlog ${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/log/${sample}.trimlog \
	${read_forward} ${read_reverse} ${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/${sample}_trimm_forward_paired.fastq.gz \
	${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/${sample}_trimm_forward_unpaired.fastq.gz \
	${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/${sample}_trimm_reverse_paired.fastq.gz \
	${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticPE/${sample}_trimm_reverse_unpaired.fastq.gz \
	ILLUMINACLIP:/usr/src/Trimmomatic/0.38/Trimmomatic-0.38/adapters/TruSeq3-PE.fa:2:30:10:2:keepBothReads \
	LEADING:3 TRAILING:3 MINLEN:36
	"""
}

/*
 Process trimmomaticSE performence preprocessing of the .fastq files 
 using the trimmomatic software of single end reads.
 
 Input:
	tuple val(index), val(sample), path(read)
		Tuple containing the index, sample ID and reads
		
 Output:
	trimm_log - path
		A log file of the trimmomatic process
	trimmed_reads - tuple val(index), val(sample), path(sample_outdir), path(trimmed_reads)
		Tuple containing the index, sample ID, sample specific output path and trimmed reads
*/		
process trimmomaticSE {
	cpus 2
	label 'trimmomatic'

	input:
	tuple val(index), val(sample), path(read)
	path output_dir
	
	output:
	path "${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticSE/log/*.trimlog", emit: 'trimm_log'
	tuple val(index), val(sample), path("${output_dir}/proteogenomics_pipeline/${sample}/"), path("${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticSE/${sample}_trimmed.fastq.gz"), emit: 'trimmed_reads'
	
	script:
	"""
	mkdir -p ${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticSE/log

	java -jar /usr/src/Trimmomatic/0.38/Trimmomatic-0.38/trimmomatic-0.38.jar SE \
	-threads ${task.cpus} -trimlog ${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticSE/log/${sample}.trimlog \
	${read} ${output_dir}/proteogenomics_pipeline/${sample}/trimmomaticSE/${sample}_trimmed.fastq.gz \
	ILLUMINACLIP:/usr/src/Trimmomatic/0.38/Trimmomatic-0.38/adapters/TruSeq3-SE.fa:2:30:10 \
	LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36
	"""
}
