#!/usr/bin/env python3

# IMPORTS
import sys
import os
import gzip
import argparse
import subprocess

__author__ = "Tessa Gillit, Yanick Paco Hagemeijer, Stijn Arends"
__version__ = "v.1"
__date__ = "20-10-2020"


def zcat(vcf_gz):
	"""
	Acts as a generator to open the input file, either zipped or not.

	:Parameter 
	----------
	vcf_gz: string
		Input vcf file
	"""
	if vcf_gz.endswith(".gz"):
		zcat_process = subprocess.Popen([ "zcat", str(vcf_gz)], stdout = subprocess.PIPE,universal_newlines=True)
		filehandle = zcat_process.stdout
	else:
		filehandle = open(vcf_gz, "r")
	for line in filehandle:
			yield line


def filterQualityVcf(input_file, output_file, qualCutoff):
	"""
	filterQualityVcf filters VCF files based on the 
	Phred-scaled probability that a REF/ALT polymorphism exists at a site.
	The filtered data will be writtin to a output file.
	
	:parameter
	----------
	input_file - file
		A file in .VCF format
	output_file - string
		The name of the output file where the filtered data will be written
		to
	qualCutOff - int
		The value that is determined which line to keepand which to discard
	"""
	# Open the output file
	out_file = open(output_file, 'wt')

	for line in zcat(input_file):

		if line.startswith("##") or line.startswith("#"):
			out_file.write(line)
		else:
			split_line = line.split('\t')
			# Grab the quality value of the line
			quality = float(split_line[5])
			# Check quality
			if quality <= float(qualCutoff):
				continue
			else:
				out_file.write(line)

	out_file.close()	


def main(input_file, output_file, qualCutoff):
	filterQualityVcf(input_file, output_file, qualCutoff)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog="filterVcfQual",
		description="Filtering of the VCF file for a minimum variant quality value",
		epilog="Contact: stijnarend@live.nl")

	# Set version
	parser.version = __version__

	parser.add_argument('input_vcf',
		help='The path to a .VCF file')

	parser.add_argument('output_file',
		help='The name of the output file that will be created')

	parser.add_argument('quality_value',
		help='The quailty cut off value')

	parser.add_argument('-v',
		'--version',
		help='Displays the version number of the script and exitst',
		action='version')

	if len(sys.argv) == 1:
		parser.print_help(sys.stderr)
		sys.exit(1)

	args = parser.parse_args()

	input_file = args.input_vcf
	output_file = args.output_file
	qualCutoff = args.quality_value

	# Check if the input file exists or not
	if os.path.isfile(input_file):
		print('File does exists: ' + input_file)
		sys.exit(main(input_file, output_file, qualCutoff))
	else:
		print('File ' + input_file + ' does not exists, exiting....')
		sys.exit(1)
