import sys
import unittest
from unittest import mock
from unittest.mock import patch, mock_open
from processPhasedVariants import ProcessPhasedVariants
from processPhasedVariants import LineHandeler
from io import TextIOWrapper, BytesIO, StringIO

class TestProcessPhasedVariants(unittest.TestCase):

	version_line = "##fileformat=VCFv4.2"

	@classmethod
	def setUpClass(cls):
		print('setupClass TestProcessPhasedVariants\n')

	def setUp(self):
		print('setup TestProcessPhasedVariants\n')

		self.processor = ProcessPhasedVariants(100, True, True)

	def tearDown(self):
		print('teardown TestProcessPhasedVariants\n')

	@patch('builtins.print')
	def test_check_vcf_version(self, mock_print):
		"""
		Test the check_vcf_version function if it prints the right line and if it 
		raises the right error.
		"""
		print("Test check_vcf_version\n")

		self.processor.check_vcf_version(self.version_line)

		mock_print.assert_called_with('Check version: VCFv4.2')

		with self.assertRaises(AttributeError):
			self.processor.check_vcf_version(2)

	def test_get_vcf_version(self):
		"""
		Test the get_vcf_version function if it returns the right string and if it 
		raises the right error.
		"""
		print("Test get_vcf_version\n")

		vcf_version = self.processor.get_vcf_version(self.version_line)
		self.assertEqual(vcf_version, 'VCFv4.2')

		with self.assertRaises(AttributeError):
			self.processor.get_vcf_version(2)


class TestLineHandeler(unittest.TestCase):

	test_line = "1\t4897866\t.\tG\tGAGAT\t41.66\t.\tAS_SB_TABLE=1,1|0,1;DP=3;ECNT=3;MBQ=20,35;MFRL=145,347;MMQ=60,60;MPOS=5;POPAF=7.30;TLOD=3.72\tGT:AD:AF:DP:F1R2:F2R1:SB\t0/1:2,1:0.500:3:2,1:0,0:1,1,0,1"
	test_phased_line = "1\t4897866\t.\tG\tGAGAT\t41.66\t.\tAS_SB_TABLE=1,1|0,1;DP=3;ECNT=3;MBQ=20,35;MFRL=145,347;MMQ=60,60;MPOS=5;POPAF=7.30;TLOD=3.72\tGT:AD:AF:DP:F1R2:F2R1:SB\t0|1:2,1:0.500:3:2,1:0,0:1,1,0,1"
	sample_anno = "0/1:2,1:0.500:3:2,1:0,0:1,1,0,1"
	geno_type = "0/1"

	@classmethod
	def setUpClass(cls):
		print('setupClass TestLineHandeler\n')

	def setUp(self):
		print('setup TestLineHandeler\n')

		self.line_handeler = LineHandeler(self.test_line)

	def tearDown(self):
		print('teardown TestLineHandeler\n')

	def test_passes_quality_filter(self):
		"""
		Test the passes_quality_filter function of the LineHandeler class.
		Check if it returns the righ boolean.
		"""

		print("Test passes_quality_filter\n")

		qual_bool = self.line_handeler.passes_quality_filter(100)

		self.assertFalse(qual_bool)

	def test_get_genotype(self):
		"""
		Test the get_genotype function of the LineHandeler class. Check if it returns the expected genotype.
		"""
		print("Test get_genotype\n")
		geno_type_val = self.line_handeler.get_genotype()

		self.assertEqual(geno_type_val, self.geno_type)

	def test_make_phased_genotype(self):
		"""
		Test the make_phased_genotype function of the LineHandeler class.
		Check if the genotype of the input line has correctly changed to be phased instead of unphased.
		"""

		print("Test make_phased_genotype\n")
		phased_genotype_line = self.line_handeler.make_phased_genotype()

		self.assertEqual(phased_genotype_line, self.test_phased_line)

if __name__ == '__main__':
	unittest.main()