#!/usr/bin/env python3

# IMPORTS
import sys
import os 
import argparse
import time
import logging
import functools
import subprocess

__author__ = "Stijn Arends"
__version__ = "v.1"
__date__ = "21-12-2020"

def timer(f):
	"""
	time decorator to meassure the time it took for a function to finish.
	"""
	def wrapper(*args, **kwargs):
		start = time.time()
		rv = f(*args, **kwargs)
		total = time.time() - start
		print(f'{f.__name__}, total time : {total}')
		return rv

	return wrapper

class LineHandeler:
	"""
	Perform various modifications to a single input line from a VCF file.
	"""

	def __init__(self, line):
		if type(line) != str:
			raise AttributeError("[ProcessPhasedVariants.get_vcf_version]: Line must be a string.")

		self.line = line
		self.cols = line.rstrip('\n').split('\t')

	def passes_quality_filter(self, qualCutOff):
		"""
		Check if the quality of the variant is below or above the quality threshold.

		:parameter 
		----------
		qualCutOff: float
			A float indicating the quality cut off value

		:returns
		--------
		True/False: boolean
			If the quality is below or equal to the qualCutOff value then return False, else return True
		"""
		qual = float(self.cols[5])

		if qual <= qualCutOff:
			return False 
		else:
			return True

	def get_genotype(self):
		"""
		Return the genotype

		:returns
		--------
		genotype: string
			The genotype of a variant
		"""
		return self.cols[9].split(':')[0] # Return genotype

	def make_phased_genotype(self):
		"""
		Transforms the unphaed genotype into a phased genotype by
		replacing the / with a |. 

		:returns
		--------
		phased line: string
			Modified line containing a phased genotype instead of a unphased genotype.
		"""
		genotype = self.get_genotype()

		phased_genotype = genotype.replace('/', '|')

		new_sample_info = self.cols[9].replace(genotype, phased_genotype)

		self.cols[9] = new_sample_info
		
		return '	'.join(self.cols)


class ProcessPhasedVariants:
	"""
	Split the phased and unphased genotypes from a VCF file into two seperate
	VCF files.
	"""

	def __init__(self, qualCutOff, filter_qual, check_vcf_tag):
		self.vcf_version = "4.2"
		self.qualCutOff = qualCutOff
		self.check_vcf_tag = check_vcf_tag
		if qualCutOff != None:
			self.filter_qual = True
		else:
			self.filter_qual = False

	def check_vcf_version(self, line):
		"""
		Check the version of the VCF file. 
		If the version doesn't match the expected version exit the program.

		An unexpected VCF version could contain a different format and would therefore crash the script.

		:parameters
		-----------
		line: string
			A string containing the information of one line from the VCF file.
		"""
		if type(line) != str:
			raise AttributeError("[ProcessPhasedVariants.check_vcf_version]: Line must be a string.")

		vcf_version = self.get_vcf_version(line)
		print(f"Check version: {vcf_version}")

		if self.vcf_version not in vcf_version:
			print(f"Program does not support VCF file format {vcf_version}. Please use version {self.vcf_version}")
			sys.exit(1)

	def get_vcf_version(self, line):
		"""
		Grab the value representing the VCF version from the line which holds the information
		of the VCF version.

		:parameters
		-----------
		line: string
			Line containing the information of the VCF version.

		:return
		-------
		vcf version: string
			A string containing the VCF file format version.
		"""
		if type(line) != str:
			raise AttributeError("[ProcessPhasedVariants.get_vcf_version]: Line must be a string.")

		return line.split('=')[1].rstrip('\n') # Return the VCF file format version of the VCF file

	def zcat(self, vcf_gz):
		"""
		Acts as a generator to open the input file, either zipped or not.

		:Parameter 
		----------
		vcf_gz: string
			Input vcf file
		"""
		if vcf_gz.endswith(".gz"):
			zcat_process = subprocess.Popen([ "zcat", str(vcf_gz)], stdout = subprocess.PIPE,universal_newlines=True)
			filehandle = zcat_process.stdout
		else:
			filehandle = open(vcf_gz, "r")
		for line in filehandle:
				yield line


	@timer
	def filterVariants(self, in_file, out_file_phased, out_file_unphased):
		"""
		Split the phased and unphased genotypes from the input VCF file into two seperate VCF files.
		Transform unphased genotypes of 1/1 and 2/2 into phased genotypes.

		:parameters
		-----------
		in_file: string
			Input VCF file
		out_file_phased: string
			Name of the ouput VCF that will contain the phased genotypes
		out_file_unphased: string
			Name of the ouput VCF that will contain the unphased genotypes
		"""
		# Open output file
		filtered_phased_file = open(out_file_phased, 'wt')
		filtered_unphased_file = open(out_file_unphased, 'wt')

		vcf_filehandle = self.zcat(in_file)
		if self.check_vcf_version:
			vcf_version_line = next(vcf_filehandle)
			vcf_version = self.check_vcf_version(vcf_version_line)
			filtered_phased_file.write(vcf_version_line)
			filtered_unphased_file.write(vcf_version_line)

		for line in vcf_filehandle:
			filtered_phased_file.write(line)
			filtered_unphased_file.write(line)
			if not line.startswith("##"):
				break
		for line in vcf_filehandle:
			line_handler = LineHandeler(line)
			try:
				if self.filter_qual and not line_handler.passes_quality_filter(self.qualCutOff):
					continue
			except ValueError:
				pass

			# Get genotype
			genotype = line_handler.get_genotype()

			if genotype in ('0/0', '0|0'):
				continue
			elif '|' in genotype:
				filtered_phased_file.write(line)
			elif genotype == '1/1' or genotype =='2/2':
				# Transform the unphased genotype to a phased genotype
				phased_line = line_handler.make_phased_genotype()
				filtered_phased_file.write(phased_line + '\n')
			else:
				filtered_unphased_file.write(line)


def main(in_vcf_file, out_file_phased, out_file_unphased, qualCutOff, filter_qual, check_vcf_tag):

	if qualCutOff != None:
		qualCutOff = float(qualCutOff)

	# Initialize class instance
	processor = ProcessPhasedVariants(qualCutOff, filter_qual, check_vcf_tag)

	processor.filterVariants(in_vcf_file, out_file_phased, out_file_unphased)

def add_arguments():
	"""
	Define the arguments, description and epilog of the script.
	"""
	parser = argparse.ArgumentParser(prog="processPhasedVariants",
		description="Program that write the phased variants of a VCF file out to a seperate VCF file." + 
		"This VCF file will be used for the haplosaurus tool.",
		epilog="Contact: stijnarend@live.nl")

	# Set version
	parser.version = __version__

	parser.add_argument('input_vcf',
		help='The path to a .VCF file')

	parser.add_argument('output_file_phased',
		help='Output file that will contain the phased variants')

	parser.add_argument('output_file_unphased',
		help='Output file that will contain the unphased variants')

	parser.add_argument('-q', '--quality',
		help='The quality cut off value',
		type=float),

	parser.add_argument('--filter_qual',
		help='Flag to turn quality filter on. Default is false',
		action='store_true')

	parser.add_argument('--check_vcf_tag',
		help='Flag to turn VCF version check on. Default is false',
		action='store_true')

	parser.add_argument('-v',
		'--version',
		help='Displays the version number of the script and exitst',
		action='version')

	if len(sys.argv) == 1:
		parser.print_help(sys.stderr)
		sys.exit(1)

	args = parser.parse_args()

	return args.input_vcf, args.output_file_phased, args.output_file_unphased, args.quality, args.filter_qual, args.check_vcf_tag

if __name__ == "__main__":
	in_vcf_file, out_file_phased, out_file_unphased, qualCutOff, filter_qual, check_vcf_tag = add_arguments()

	# Check if the input file exists or not
	if os.path.isfile(in_vcf_file):
		sys.exit(main(in_vcf_file, out_file_phased, out_file_unphased, qualCutOff, filter_qual, check_vcf_tag))
	else:
		print('File ' + in_vcf_file + ' does not exists, exiting....')
		sys.exit(1)
