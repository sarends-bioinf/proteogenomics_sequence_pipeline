#!/usr/bin/env python3

# IMPORTS
import sys
import json
import operator
import collections
import itertools
from pathlib import Path
import argparse

__author__ = "Tessa Gillit, Yanick Paco Hagemeijer, Stijn Arends"
__version__ = "v.1"
__data__ = "6-11-2020"


class ProcessVEP:

	orfChanges = set([ 'stop_gained', 'frameshift_variant', 'stop_lost', 'start_lost', 'protein_altering_variant',
	'stop_retained_variant', 'start_retained_variant',
	'splice_acceptor_variant', 'splice_donor_variant',
	'incomplete_terminal_codon_variant',
	])

	aaChanges = set([ 'inframe_insertion', 'inframe_deletion', 'missense_variant',
	])

	unusedChanges = set([
		'synonymous_variant',
		'splice_region_variant',
		'intron_variant',
		## Additional flags
		'NMD_transcript_variant',
		'coding_sequence_variant',
	])

	def  __init__(self):
		pass

	# ------ VEP PARSER ------ #

	def is_data_line(self, line):
		"""
		Check if the line isn't a header.

		:parameter
		----------
		line - string
			A line from a input file

		:return
		-------
		True/False - boolean
			True if it is not a header. False if it is a header.
		"""
		return line[0] != "#"


	def is_transcript_line(self, line_cols):
		"""
		Check if the line is a transcript line.

		:parameter
		----------
		line_cols - list
			A list of a splitted line

		:return
		-------
		True/False - boolean
			True if it is a transcript line. False if it is not
		"""
		return line_cols[5] == 'Transcript'


	def vep_iter(self, fasta_file):
		"""
		yield individual transcript blocks as a whole (prevents having to handle the last entry after the loop)

		:parameter
		----------
		fasta_file - file
			Fasta file
		"""
		get_transcipt_id = operator.itemgetter(4)
		# Grab all the lines that contain data
		fasta_lines = filter(self.is_data_line, fasta_file)
		split_lines = map(lambda line: line.split("\t"), fasta_lines)
		# Grav all the lines that contain transcript information
		split_transcript_lines = filter(self.is_transcript_line, split_lines)
		return itertools.groupby(split_transcript_lines, get_transcipt_id)

	# ------ FASTA PARSER ------ #

	def is_fasta_header(self, line):
		"""
		Check if the input line is a fasta header.

		:parameter
		----------
		line - string
			Line of a input file

		:return
		-------
		True/False - boolean
			True if it is a fasta header, false if it is not.
		"""
		return line[0] == '>'

	def fasta_iter(self, fasta_file): ## https://www.biostars.org/p/710/
		"""
		Groups lines together by the boolean value of it being a header line or not
		This means a header line will have True value, and the run of non-header lines after 
		will be grouped until the boolean changes again for the next header

		:parameter
		----------
		fasta_file - File
			Input fasta file
		"""
		return itertools.groupby(fasta_file, self.is_fasta_header)

	def fasta_as_pairs(self, fasta_file):
		"""
		Load fasta files this way so we needn't worry about:
		The last entry after the last iteration or incrementally building the sequence in a loop
		"""
		# group the lines together that are not a header
		for isHeader, group_obj in self.fasta_iter(fasta_file):
			grouped_lines = tuple(group_obj)
			if isHeader:
				if len(grouped_lines) > 1: # Check if there is more then 1 title
					sys.exit("The fasta file contains 2 consecutive titles!")
				else:
					seqID = grouped_lines[0] # there is only one title entry
					sequence = '' # shouldn't matter, but just to be sure that we don't ever accidentaly return the previous sequence if there is a missing sequence
			else:
				sequence = ''.join(line.strip() for line in grouped_lines)
				yield seqID, sequence

	def get_transcriptID(self, header_line):
		"""
		Get the transcript ID
		"""
		return header_line[1:].split('.', 1)[0].split(' ', 1)[0] # skip >, the . is expected to occur before the " ", so hopefully we skip some needless iterating

	def get_protID(self, header_line):
		# Grab the prot ID
		return header_line.split(' ', 5)[4].rsplit(':', 1)[-1].split('.', 1)[0]

	def make_fasta_dict(self, fasta_file_path, f_get_ID):
		"""
		Create a dictionary with the header and all sequence lines that belong to that header
		"""
		fasta_file = fasta_file_path.open('r')

		seq_dict = {
			f_get_ID(fasta_header) : (fasta_header.lstrip(">"), sequence)
				for fasta_header, sequence in self.fasta_as_pairs(fasta_file)
		}
		fasta_file.close()
		return seq_dict

	# ----- MISC FUNCTIONS ----- #

	def sort_lines(self, line_cols):
		return line_cols[7].split('/', 1)[0] # split the line cols


	def count_lowercase_characters_at_start(self, seq):
		"""
		Count the lowercase characters

		:parameter
		----------
		seq - string
			Sequence

		:return
		-------
		lowercaseCount - int
			Amount of lower case characters
		"""
		lowercaseCount = 0
		for letter in seq:
			if letter == letter.upper():
				break
			lowercaseCount += 1
		return lowercaseCount


	# ------ DATA PROCESSING ---- #

	def combineVarsProt(self, variants, sequences, log_file):
		"""
		:parameters
		-----------
		variants - list
			Variant information
		sequences - list
			Peptide sequence
		log_file - file
			Log file

		:return
		-------
		sequences - list
			Updated sequence list
		"""
		orig_seqID, orig_seq = sequences[0]
		for variant in variants:
			variant_list = variant[10].split('/')
			if len(variant_list) == 1: #synonymous
				old = variant_list[0]
				new = variant_list[0]
			else:
				old, new = variant_list # ref allele, alt allele
			variant_parts = variant[9].split('-')
			if 1 <= len(variant_parts) <= 2: # sanity check
				var_pos = int(variant_parts[0])
				start_pos = max(var_pos-1, 0)
				if old in ('-', orig_seq[start_pos:start_pos+len(old)]):
					seq_count = len(sequences)
					for seq_list_index in range(seq_count): # work by index because we are updating the original list as we go
						seqID, seq = sequences[seq_list_index]
						if new == '-': # deletion
							new_seq = seq[:start_pos] +       seq[start_pos + len(old) :]
						elif old == '-': # insertion
							new_seq = seq[:start_pos] + new + seq[start_pos            :]
						else:
							new_seq = seq[:start_pos] + new + seq[start_pos + len(old) :]
						new_id = seqID + ':' + variant[0]
						sequences.append((new_id, new_seq)) # add seq with this var
				else:
					""""
					--------------------------------------NOTICE!-----------------------------------------
					Writing sequences that cause an error out to a log file instead of exiting the program.
					This is temporary and has to change in the future.

					Originally there was no log_file and the sys.exit was unhashed.
					"""
					print(orig_seqID, len(orig_seq), start_pos, orig_seq[ start_pos : start_pos + len(old) ], old.upper(), sep="\t")
					log_file.write('{0} \t {1} \t {2} \t {3} \t {4} \n'.format(orig_seqID, len(orig_seq), start_pos, orig_seq[start_pos:start_pos+len(old)], old.upper()))
					# sys.exit('ERROR: incorrect sequence')
					continue
			else:
				print(variant_parts)
				sys.exit('ERROR: too many positional arguments, this should not happen')
		return sequences

	def combineVarsORF(self, variants, sequences, log_file):
		"""
		:parameters
		-----------
		variants - list
			Variant information
		sequences - list
			Nucleotide sequence
		log_file - file
			Log file

		:return
		-------
		sequences - list
			Updated sequence list
		"""
		orig_seqID, orig_seq = sequences[0]
		for variant in variants:
			# get ref/alt from codon change --> takes strandedness into account
			old, new = variant[11].split('/') # ref allele, alt allele
			variant_parts = variant[7].split('-')
			seq_count = len(sequences)
			for seq_list_index in range(seq_count): # work by index because we are updating the original list as we go
				seqID, seq = sequences[seq_list_index]
				if 1 <= len(variant_parts) <= 2:
					# FIND THE RIGHT POSITION TO INSERT THE VARIANT CODON:
					lowercaseCountNew = self.count_lowercase_characters_at_start(new)
					lowercaseCountOld = self.count_lowercase_characters_at_start(old)
					var_pos = int(variant_parts[0]) - lowercaseCountNew
					if len(new) == lowercaseCountNew: # deletion: change repositioning of variant.
						var_pos += lowercaseCountNew - lowercaseCountOld
					elif len(old) == lowercaseCountOld:	# insertion: change repositioning of variant.
						# added rather than replaced --> insert one position further from start of seq
						var_pos += 1
					# SANITY CHECK: REPLACING THE SEQUENCE EXPECTED?
					start_pos = max(var_pos-1, 0)
					if old.upper() in ('-', orig_seq[start_pos:start_pos+len(old)]):
						# Check for those cases where the same variant is reported twice
						if 1:
							#orig_seq[start_pos:start_pos+len(new)] != new.upper():
							#orig_seq[var_pos:var_pos+len(new)] != new.upper():
							# REPLACE --> CREATE VARIANT SEQUENCE
							prefix = seq[:start_pos]
							if new == '-': # deletion
								new_seq = prefix +               seq[start_pos + len(old) :]
							elif old == '-': # insertion
								new_seq = prefix + new.upper() + seq[start_pos            :]
							else: # might still be an indel, e.g. atc/atGc, or something else
								new_seq = prefix + new.upper() + seq[start_pos + len(old) :]
							new_id = seqID + ':' + variant[0]
							sequences.append((new_id, new_seq)) # add seq with this var
					else:
						""""
						--------------------------------------NOTICE!-----------------------------------------
						Writing sequences that cause an error out to a log file instead of exiting the program.
						This is temporary and has to change in the future.

						Originally there was no log_file and the sys.exit was unhashed.
						"""
						print(orig_seqID, len(orig_seq), start_pos, orig_seq[start_pos:start_pos+len(old)], old.upper(), sep="\t")
						log_file.write('{0} \t {1} \t {2} \t {3} \t {4} \n'.format(orig_seqID, len(orig_seq), start_pos, orig_seq[start_pos:start_pos+len(old)], old.upper()))
						# sys.exit('ERROR: not replacing the expected sequence')
						continue
				else:
					sys.exit('ERROR: too many positional arguments, this should not happen')		
		return sequences

	def changeSeqs(self, prevTranscriptID, haps, ORF_change, dict, alreadySeenSeqIDs, editedPepFile, editedORFfile, log_file_out):
		"""
		:parameters
		-----------
		prevTransciptID - string
			Transcript ID
		haps - list
		dict - dictionary
			cDNA dictionary
		alreadySeenSeqIDs - set
			Already seen sequence IDs
		editedPepFile - file
			Output peptide file
		editedORFfile - file
			Output ORF file
		log_file_out - file
			Log file
		"""
		if ORF_change:
			originalID, originalSeq = cDNA_dict[prevTranscriptID]
			fileOut = editedORFfile
			combine_func = self.combineVarsORF
			format_header = '>VEPcdna_{transcriptID}\n'.format
		else:
			originalID, originalSeq = pep_dict[prevTranscriptID]
			fileOut = editedPepFile
			combine_func = self.combineVarsProt
			format_header = '>VEPprot_{transcriptID}\n'.format

		original = (originalID.split(' ')[0], originalSeq)
		for hap in haps:
			# get variant sequences; protSeq = [transcriptHeader, sequence], hap = VEP combinations
			# returns: [[id, seq], [id, seq], [id, seq], etc.]
			editedProtSeqs = combine_func( hap, [original], log_file_out )

			# write sequences to file
			for transcriptID, editedSeq in editedProtSeqs:
				if transcriptID == originalID: # skip first entry for transcript, this has no vars
					continue
				elif transcriptID in alreadySeenSeqIDs:
					continue
				else:
					alreadySeenSeqIDs.add(transcriptID) # remove duplicate titles
					headerLine = format_header(transcriptID=transcriptID)
					fileOut.write(headerLine)
					for seq_index in range(0, len(editedSeq), 80):
						fileOut.writelines((editedSeq[seq_index:seq_index+80], '\n'))

	def getVariantConsequences(self, transcript_lines):
		"""
		Find whether sequence needs to be edited on nucleotide or protein level.

		:parameter
		----------
		transcript_lines - string
			Lines of transcripts
		"""
		orfChange = False
		aaChange = False

		changes = set()
		for tLine in transcript_lines:
			if '*' in tLine[10]: # stop codon involved
				orfChange = True
			changes.update(tLine[6].split(','))
		# Check if event is in both changes and orfChanges
		orfs_events = ProcessVEP.orfChanges & changes
		# Remove events that are in both sets from changes
		changes -= orfs_events
		# Check if event is in both changes and aaChanges
		aa_events = ProcessVEP.aaChanges & changes
		# Remove events that are in both sets from changes
		changes -= aa_events
		if orfChange:
			pass
		elif orfs_events:
			orfChange = True
		elif aa_events:
			aaChange = True

		# Check for final changes with the unusedChanges set
		changes -= ProcessVEP.unusedChanges
		if changes:
			sys.exit("Unknown changes were detected!\n\t" + "\n\t".join(sorted(changes)))
		return orfChange, aaChange

	def getMiniHaploTypes(self, transcriptLines, max_nucleotide_distance=90):
		"""
		Get 'minihaplotypes' of all possible combinations of observed variants within 30 amino acids distance from each other.

		:parameters
		----------
		transcript_lines - string
			Lines of transcripts
		max_nucleotide_distance - int
			Max nucleotide distance, default = 90

		:return
		-------
		haps - list
			Mini haplotypes of a transcript
		"""
		haps = []
		for i in range(len(transcriptLines)):
			tPos = int(transcriptLines[i][7].split('-')[0]) # relative position in peptide
			thisHap = [transcriptLines[i]] # first var in minihap
			for transcriptLine in transcriptLines[i+1:]:
				if abs(int(transcriptLine[7].split('-')[0]) - tPos) < max_nucleotide_distance: # stop of out of 30 aa range
					thisHap.append(transcriptLine)
				else:
					break

			thisHap.sort(key = self.sort_lines, reverse=True) # reverse order of variants in minihap to prevent problems with changed positions after indels etc.
			if thisHap not in haps:
				haps.append(thisHap)
		return haps


def main(sequence_type, cDNA_file, peptide_file, VEP_infile, peptide_outfile, nucleotide_outfile, FPKM_cutoff, fpkmJson_file):
	global cDNA_dict, pep_dict

	cdna_file_path, peptide_file_path, vep_file_path = [Path(argument).resolve() for argument in (cDNA_file, peptide_file, VEP_infile)]
	edited_pep_filepath, edited_ORF_filepath = [ Path(argument) for argument in (peptide_outfile, nucleotide_outfile) ]

	# Initiate class instance
	process_vep = ProcessVEP()

	cDNA_dict = process_vep.make_fasta_dict(cdna_file_path, process_vep.get_transcriptID)
	pep_dict = process_vep.make_fasta_dict(peptide_file_path, process_vep.get_protID)
	vep_file = vep_file_path.open("r")

	if sequence_type.upper() == "RNA":
		stringtie_json_file = Path(fpkmJson_file).resolve().open("r")
		fpkmDict = json.load(stringtie_json_file) # json file with FPKMs per sequence (from stringtie)
		stringtie_json_file.close()

		fpkmCutoff = float(FPKM_cutoff) # user defined cutoff
		fpkmFilteredEntries = set()
		# Put the IDs in a set collection if they are above the FPKM cut off value
		for key, value in fpkmDict.items():
			if value >= fpkmCutoff:
				fpkmFilteredEntries.add(key)
	else:
		class fake_set:
			def __contains__(self, x):
				return True
		fpkmFilteredEntries = fake_set()

	editedPepFile = edited_pep_filepath.open('w')
	editedORFfile = edited_ORF_filepath.open('w')

	# Create a callable object that fetches item from its operand
	get_columns_7_10 = operator.itemgetter(7,10)

	alreadySeenSeqIDs = set()
	orfAaCountTrue = 0
	orfAaCountAll = 0
	iter_count = 0
	log_file = editedPepFile.name + "_log.txt"
	log_file_out = open(log_file, 'w')
	for transcriptLineCount, ((transcriptID), transcriptLines_object) in enumerate(process_vep.vep_iter(vep_file), start=1):
		iter_count += 1
		raw_transcriptLines = list(transcriptLines_object)

		if transcriptID not in fpkmFilteredEntries:
			continue

		# checks cdna position and codon change
		# same for those without any codon changes
		# skip variants that don't provide a position in the cDNA seq --> not coding
		transcriptLines = [ tLine for tLine in raw_transcriptLines if "-" not in get_columns_7_10(tLine) ]

		if transcriptLines:
			orfAaCountAll += 1
			if len(transcriptLines) > 1:
				print(transcriptID, "\tNth Transcript:", transcriptLineCount, "# of Transcript lines:", len(raw_transcriptLines), "After filtering:", len(transcriptLines), sep="\t")
			orfChange, aaChange = process_vep.getVariantConsequences(transcriptLines) # 

			if orfChange or aaChange:
				orfAaCountTrue += 1
				
				# CHECK FOR 'MINI HAPLOTYPES' for this transcript
				haps = process_vep.getMiniHaploTypes(transcriptLines)
				# FOR THIS TRANSCRIPT: store for transdecoder or directly obtain changed pep seq
				process_vep.changeSeqs(transcriptID, haps, orfChange, cDNA_dict, alreadySeenSeqIDs, editedPepFile, editedORFfile, log_file_out)

	editedPepFile.close()
	editedORFfile.close()
	vep_file.close()
	print(transcriptLineCount, '|' ,orfAaCountAll, '|', orfAaCountTrue)


def add_arguments():
	parser = argparse.ArgumentParser(prog="processVEPoutput",
		description="processingVEP predictions of variants to either modify amino acids " +
		"in the corresponding protein sequence, or modify nucleotides as part of the " +
		"corresponding transcript sequence after which a new ORF must be predicted.",
		epilog="Contact: stijnarend@live.nl",
		usage="%(prog)s [-h] [-v] DNA cDNA_file peptide_file, VEP_infile, peptide_outfile, nucleotide_outfile " +
		"or %(prog)s [-h] [-v] RNA cDNA_file peptide_file, VEP_infile, peptide_outfile, nucleotide_outfile, FPKM_cutoff, fpkmJson_file")

	# set version
	parser.version = __version__

	# Add arguments
	parser.add_argument('sequence_type', 
		help="Sequence type: DNA or RNA",
		choices=['DNA', 'RNA'])
	parser.add_argument('cDNA_file', 
		help="This file holds the cDNA sequences corresponding to Ensembl gene predictions.")
	parser.add_argument('peptide_file',
		help="Ensembl peptide sequence reference file")
	parser.add_argument('VEP_infile',
		help="File produced by VEP")
	parser.add_argument('peptide_outfile',
		help="Peptide output .fa file")
	parser.add_argument('nucleotide_outfile',
		help="Nucleotide output .fa file.")
	parser.add_argument('--FPKM_cutoff',
		help="The FPKM cut off value. Required when sequence type is RNA",
		type=float)
	parser.add_argument('--fpkmJson_file',
		help="The FPKM json file produced by the processStringTieOutput.py script. Required when sequence type is RNA")

	parser.add_argument('-v',
		'--version',
		help='Displays the version number of the script and exitst',
		action='version')

	# Check if any arguments are supplied
	if len(sys.argv) == 1:
		parser.print_help(sys.stderr)
		sys.exit(1)

	args = parser.parse_args()	

	# Check for sequence type
	if args.sequence_type.upper() not in ('DNA', 'RNA'):
		print(f"Please provide a valid sequence type(DNA or RNA)")
		parser.print_help(sys.stderr)
		sys.exit(1)

	if args.sequence_type.upper() == 'RNA' and args.FPKM_cutoff == None and args.fpkmJson_file == None:
		print("Attention! Please provide an FPKM cut off value and a FPKM jason file when selecting RNA as sequence type")
		print(parser.print_usage())

	return args.sequence_type, args.cDNA_file, args.peptide_file, args.VEP_infile, args.peptide_outfile, args.nucleotide_outfile, args.FPKM_cutoff, args.fpkmJson_file


if __name__ == "__main__":
	sequence_type, cDNA_file, peptide_file, VEP_infile, peptide_outfile, nucleotide_outfile, FPKM_cutoff, fpkmJson_file = add_arguments()
	sys.exit(main(sequence_type, cDNA_file, peptide_file, VEP_infile, peptide_outfile, nucleotide_outfile, FPKM_cutoff, fpkmJson_file))