#!/usr/bin/env python3

# IMPORTS
import sys
import argparse


__author__ = "Tessa Gillit, Yanick Paco Hagemeijer, Stijn Arends"
__version__ = "v.1"
__data__ = "5-11-2020"


class SpliceJunctions:

	def __init__(self):
		pass

	def loadSequences(self, seq_file):
		"""
		Processes the exon sequence file.

		:parameter
		----------
		seq_file - string 
			File containing the exon seqs

		:returns
		--------
		posistions_dict - dictionary
			Dictionary which holds chromosome begin and end of a exon
		sequence_dict - dictionary
			Dictionary for each chromosome which holds transcript id, begin, end and sequence
		"""
		positions_dict = {}
		sequence_dict = {}
		first_line = True

		with open(seq_file) as seqf:
			count = 0
			for line in seqf:
				# remove empty white spaces
				line = line.rstrip()
				count += 1
				if line[0] == '>':
					if first_line != True:
						# store previous sequence
						# Create a dioctionary for each chromosome which will hold
						# transcript id, begin, end and sequence
						if chrom not in sequence_dict.keys():
							sequence_dict[chrom] = {}
						if chrom not in positions_dict.keys():
							positions_dict[chrom] = {'endExon': {}, 'beginExon': {}}

						sequence_dict[chrom][transcriptID] = {
							'begin': begin,
							'end': end,
							'seq': sequence}

						positions_dict[chrom]['endExon'][end] = transcriptID
						positions_dict[chrom]['beginExon'][begin] = transcriptID
					
					else:
						first_line = False

					# get info new sequence
					line_split = line.split('_')
					transcriptID = line_split[4]
					chrom = line_split[0][1:]
					begin = int(line_split[1])
					end = int(line_split[2])
					strand = line_split[3]
					sequence = ""

				else:
					sequence += line

		return (positions_dict, sequence_dict)

	def processSJfile(self, sj_file, positions_dict, sequence_dict):
		"""
		Process the splice junctions table produced by STAR.

		:parameters
		-----------
		sj_file - string
			Tab-delimited splice junctions file
		posistions_dict - dictionary
			Dictionary which holds chromosome begin and end of a exon
		sequence_dict - dictionary
			Dictionary for each chromosome which holds transcript id, begin, end and sequence
		
		:returns
		--------
		seqList - list
			List containing lists which contain the header new nucl sequence and strand
		"""
		with open(sj_file) as alignm:
			seqList = []
			count = 0
			for line in alignm:
				count += 1
				line = line.rstrip()

				split_line = line.split('\t')

				if int(split_line[6]) < 2 or split_line[5] != '0': # skip singleton reads, annotated junctions
					continue

				chrom, start_intron, end_intron, strand = split_line[0], int(split_line[1]), int(split_line[2]), split_line[3] # 1 based

				both_found = False
				if chrom in positions_dict.keys():
					if start_intron - 1  in positions_dict[chrom]['endExon'].keys():
						leftExonID = positions_dict[chrom]['endExon'][start_intron-1]
						if end_intron + 1  in positions_dict[chrom]['beginExon'].keys():
							rightExonID = positions_dict[chrom]['beginExon'][end_intron+1 ]
							both_found = True
				else:
					print(chrom, 'not found in ' + sj_file)

				if both_found != True:
					continue

				newNuclSeq = sequence_dict[chrom][leftExonID]['seq'] + sequence_dict[chrom][rightExonID]['seq']
				header = '>SJ_' + leftExonID + ':' + rightExonID
				seqInfo = [header, newNuclSeq, strand]
				seqList.append(seqInfo)
			return seqList

	def translate(self, seq):
		"""
		Translate the nucleotides to aminoAcids

		:parameter
		----------
		seq - string
			Nucleotide sequences 

		:return
		paptides - string
			Nucleotides transformed into aminoacids to form a peptide
		"""
		bases = "tcag"
		codons = [a + b + c for a in bases for b in bases for c in bases]
		aminoAcids = 'FFLLSSSSYY**CC*WLLLLPPPPHHQQRRRRIIIMTTTTNNKKSSRRVVVVAAAADDEEGGGG'
		codonTable = dict(zip(codons, aminoAcids))
		seq = seq.lower()
		peptide = ''
		
		for i in range(0, len(seq), 3):
			codon = seq[i: i+3]
			amino_acid = codonTable.get(codon, '*')
			if amino_acid != '*':
				peptide += amino_acid
			else:
				break
					
		return peptide


	def litranslate3or6FT(self, seq, strand):
		"""
		Create a list of the peptide sequences

		:parameters
		-----------
		seq - string
			Sequence
		strand - int
			Strand (0 = undefined, 1 = +, 2 = -)

		:returns
		--------
		pep_seqs - list
			List containing peptides
		header_info - list
			List containing the header info
		"""
		seqsForw = [seq, seq[1:], seq[2:]]
		seqsRev = [seq[::-1], seq[::-1][1:], seq[::-1][2:]]
		seqs6FT = seqsForw + seqsRev

		if strand == 0:
			seqsToTranslate = seqs6FT
			header_info = ['forw0', 'forw1', 'forw2', 'rev0', 'rev1', 'rev2']
		elif strand == 1:
			seqsToTranslate = seqsForw
			header_info = ['forw0', 'forw1', 'forw2']
		elif strand== 2:
			seqsToTranslate = seqsRev
			header_info = ['rev0', 'rev1', 'rev2']
		
		pep_seqs = []
		for s in seqsToTranslate:
			pep_seqs.append(self.translate(s))

		return(pep_seqs, header_info)


def main(seq_file, splice_junctions_tab, outfile_spliced_exons):
	out_file = open(outfile_spliced_exons, 'w')
	out_stats = open(outfile_spliced_exons + '_logStats.txt', 'w')

	count30Length = 0
	count30Length0 = 0
	count30Length1 = 0
	count30Length2 = 0
	countBelow30Length = 0
	uniqueAbove30Length = 0

	# classify class instance
	splice_junctions = SpliceJunctions()
	positions_dict, sequence_dict = splice_junctions.loadSequences(seq_file)

	seq_list =splice_junctions.processSJfile(splice_junctions_tab, positions_dict, sequence_dict)

	out_stats.write(str(len(seq_list)) + 'unknown splice junctions to be translated')

	for seq_info in seq_list:
		pep_seqs, header_info = splice_junctions.litranslate3or6FT(seq_info[1], int(seq_info[2]))

		for i in range(len(pep_seqs)):
			oneAbove30 = False

			if len(pep_seqs[i]) >= 30:
				count30Length += 1

				if int(seq_info[2]) == 0:
					count30Length0+=1
				if int(seq_info[2]) == 1:
					count30Length1+=1
				if int(seq_info[2]) == 2:
					count30Length2+=1

				oneAbove30 = True

				out_file.write(seq_info[0] + header_info[i] + '\n')
				pep_seq = pep_seqs[i]
				while len(pep_seq) > 0:
					out_file.write(pep_seq[:80] + '\n')
					pep_seq = pep_seq[80:]
			else:
				countBelow30Length += 1

		if oneAbove30 == True:
			uniqueAbove30Length += 1

	out_stats.write('Of which' + str(uniqueAbove30Length) + 'contain at least one translated ORF of >30 aa')
	out_stats.write('In total'+ str(count30Length) + 'translated sequences of >30 aa out of' + 
		str(countBelow30Length + count30Length) + '(' + str(count30Length / (countBelow30Length + count30Length)) + '%)')
	out_stats.write('Unstranded:' + str(count30Length0) + ', forw:' + str(count30Length1) + ', reverse:' + str(count30Length2))


def add_arguments():
	parser = argparse.ArgumentParser(prog="spliceJuntions",
		description="combine  alignment  information  and  exon  sequences to include alternative splice sites of annotated transcripts.",
		epilog="Contact: stijnarend@live.nl")

	# Set version
	parser.version = __version__

	parser.add_argument('sequence_file',
		help='A .fa file containing sequences')

	parser.add_argument('splice_junctions_tab',
		help='Splice junction tables')

	parser.add_argument('outfile_spliced_exons',
		help='The path and name of a output .fa file that will contain the spliced exones')

	parser.add_argument('-v',
		'--version',
		help='Displays the version number of the script and exitst',
		action='version')

	if len(sys.argv) == 1:
		parser.print_help(sys.stderr)
		sys.exit(1)

	args = parser.parse_args()

	return args.sequence_file, args.splice_junctions_tab, args.outfile_spliced_exons


if __name__ == "__main__":
	seq_file, splice_junctions_tab, outfile_spliced_exons = add_arguments()
	sys.exit(main(seq_file, splice_junctions_tab, outfile_spliced_exons))
