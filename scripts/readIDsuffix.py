#!/usr/bin/env python3

# IMPORTS
import sys
import argparse
import subprocess

__author__ = "Tessa Gillit, Yanick Paco Hagemeijer, Stijn Arends"
__version__ = "v.1"
__data__ = "18-11-2020"


def addSuffix(filename, suffix):
	"""
	Add either a /1 or /2 suffix to the read IDs

	:parameter
	----------
	filename - string
		Input .fq file
	suffix - string
		either /1 or /2
	"""
	# Define outputfile name
	filenameOut = filename + '.suffixed.fq'
	linecount = 0

	with open(filename) as f:
		with open(filenameOut, 'w') as f_out:
			for line in f:
				line = line.rstrip()
				if linecount % 4 == 0:
					splitLine = line.split(' ')
					editedLine = splitLine[0] + suffix + ' ' + ' '.join(splitLine[1:]) + '\n'
					f_out.write(editedLine)

				elif linecount % 4 == 2:
					if len(line) > 1:
						editedLine = line + suffix + '\n'
						f_out.write(editedLine)
					else:
						f_out.write(line + '\n')
				else:
					f_out.write(line  + '\n')

				linecount += 1

def add_arguments():
	parser = argparse.ArgumentParser(prog="readIDsuffix",
		description="Used to append /1 or /2 suffix to read IDs as required by Trinity..",
		epilog="Contact: stijnarend@live.nl")

	# Set version
	parser.version = __version__

	parser.add_argument('in_file',
		help='A .fq file containing unmapped reads')

	parser.add_argument('suffix',
		help='Suffix: /1 or /2',
		choices=['/1', '/2'])

	parser.add_argument('--no_check',
		help="Won't check for .fq extension",
		action='store_true')

	parser.add_argument('-v',
		'--version',
		help='Displays the version number of the script and exitst',
		action='version')

	if len(sys.argv) == 1:
		parser.print_help(sys.stderr)
		sys.exit(1)

	args = parser.parse_args()

	return args.in_file, args.suffix, args.no_check

if __name__ == "__main__":
	filename, suffix, no_check = add_arguments()
	if not no_check:
		if filename[-3:] != '.fq':
			print(f"Input file: {filename} does not contain an .fq extension, please provide a correct input file...")
			sys.exit(1)

	if suffix != '/1' and suffix != '/2':
		print(f"Please specify either /1 or /2 as suffix..")
		sys.exit(1)

	addSuffix(filename, suffix)
