#!/bin/bash

set -euxo pipefail;

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

release="102";
url="http://ftp.ensembl.org/pub/release-${release}/fasta/";

progress_file="./ensembl_species_genome_builds.list";
rm --force "${progress_file}";
for species in $(wget -O - "http://ftp.ensembl.org/pub/release-102/fasta/" 2> /dev/null | grep -P -o '^<a href="[^/]+' | grep -P -o '[^"]+$' | grep -v -P "ancestral[-_]alleles" | tac | tac;);
do
	checksum_url="${url}/${species}/dna/CHECKSUMS";
	genome_build="$(wget -O - "${checksum_url}" 2> /dev/null | grep -o -P "\..+\.dna_sm\.toplevel\.fa\.gz" | sed -r -e 's/\.dna_sm\.toplevel\.fa\.gz//' -e 's/^\.//g' | tr -d '\n';)";
	echo -e "${species}\t${release}\t${genome_build}" | tee -a "${progress_file}";
	~/nextflow run "${SCRIPTPATH}/../../reference_prepping.nf" -resume --species "${species}" --genome_build "${genome_build}" --release "${release}";
done