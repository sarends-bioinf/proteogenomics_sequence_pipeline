#!/usr/bin/env nextflow

/*
 * Copyright (c) 2020, European Research Institute 
 * for the Biology of Ageing(ERIBA)<http://eriba.umcg.nl/>
 * Department of the aging biologyha
*/

/*
 * A proteogenomics data integration pipeline implemented in nextflow
 *
 * Authors:
 * - Stijn Arends <stijnarends@live.nl>
 *
 * Version:
 * - v1.0
 *
 * Date:
 * - 2-10-2020
*/

// enables modules
nextflow.enable.dsl = 2

// Assign a begin value to avoid warning message
params.help            = false
params.resume          = false
params.monochrome_logs = false

def helpMessage() {
	log.info nfcoreHeader()
	log.info """\
	\n
	Usage:
		Typical command for running the pipeline goas as follows:
		
		nextflow run main.nf --reference </dir/to/store/files> --species <species name> --release <release-101> --sequence_length <length of the reads>
		--end <paired or single end> 
	
	Description:
		Version 1.0
		
		This is the main pipeline. It collects and distributes data from and to other
		nextflow scripts, which will perform various tasks. Including preprocessing and
		quality control of the data, mapping the data to a genome and postprocessing( creation
		of a protein database). It uses the same nextflow.config script used in the 
		reference_prepping.nf script in order to know where the the downloaded files 
		and genome index are.
	
	Options:
		--help
			Display the help message to the screen
			
		--monochrome_logs 
			Disable the colors of the banner
		
		Sequence read information:
			--sequence_length <integer>
				Length of a sequence read. Will be used to calculate the --sjdbOverhang 
				parameter of STAR
				
			--end <string>
				Either select PE for paired end reads or SE for single end reads

			--seq_type <string>
				Sequence type, either select DNA or RNA
		
		Genome info:
			--species <string>
				Name of the species that will be used to download the the reference files
				file
				
			--release <string>
				Specify which Ensembl API release will be used to download the the reference files

			--genome_buiild <string>
				Specify genome build that will be used to download the reference files
			
		Output:
			--outdir <string>
				Specify the location where the results will be generated 
		
			--reference <string>
				Specify the location where the files will be downloaded en the index will be
				build
		
		Data:
			--data_dir <string>
				Specify the location of the input files

		Filters:
			--vcfQual <integer>
				Quality cutoff value for processing the VCF files.

			--fpkm <string>
				FPKM filter
				
	""".stripIndent()
}

// If help parameter exsists then display the helpmessage
if (params.help) exit 0, helpMessage()

log.info nfcoreHeader()

if (params.resume) exit 1, "Are you making the classical --resume typo? Be careful!!!! ;)"

 
/*--------------*
* IMPORT MODULES
*---------------*/
include {PREPROCESSING} from './modules/preprocessing_QC'
include {MAPPING} from './modules/mapping'
include {PROTEIN_DB} from './modules/protein_DB'


/*-------------*
* MAIN WORKFLOW
*--------------*/
/*
 The main workflow distributes and collects data to and from
 workflows inside other nexflow scripts that are included.
*/
workflow {
	// collect samples from config file
	def samples = params.samples.keySet().collect()
	def items_forward = []
	def items_reverse = []
	for (sample in samples) {

		if ( params.end == "PE" ){
			items_forward.add(params.samples.get(sample).get('RNA_seq_1'))
			items_reverse.add(params.samples.get(sample).get('RNA_seq_2'))
		}else if ( params.end == "SE") {
			items_forward.add(params.samples.get(sample).get('RNA_seq_1'))
		}

	}
	
	samples_ch = Channel.from(samples)
	files_forward_ch = Channel.from(items_forward)
	files_reverse_ch = Channel.from(items_reverse)

	// index the channels.
	a=0; files_forward_ch.map{[a++, it]}.set{keyed_files_forward_ch}
	b=0; files_reverse_ch.map{[b++, it]}.set{keyed_files_reverse_ch}
	c=0; samples_ch.map{[c++, it]}.set{keyed_samples_ch}

	samples_files = keyed_samples_ch.join(keyed_files_forward_ch, by: 0)

	if ( params.end == "PE" ){
		samples_files = samples_files.join(keyed_files_reverse_ch, by: 0)
	}

	// preprocess the input reads
	PREPROCESSING( samples_files )

	// mapped the trimmed reads
	MAPPING( PREPROCESSING.out.trimm_reads )

	// create a peptide sequence database
	PROTEIN_DB( MAPPING.out.aligned_reads, MAPPING.out.indexed_reads, MAPPING.out.splice_juntion_tab,
	MAPPING.out.unmapped_reads, MAPPING.out.unmapped_reads_2, PREPROCESSING.out.trimm_unpaired_reads)
	
}

def nfcoreHeader() {
	// Log colors ANSI codes
    c_blueish   = params.monochrome_logs ? '' : "\033[38;5;21m";
    c_greenish  = params.monochrome_logs ? '' : "\033[38;5;119m";
    c_purpleish = params.monochrome_logs ? '' : "\033[38;5;93m";

    c_dim   = params.monochrome_logs ? '' : "\033[2m";
    c_reset = params.monochrome_logs ? '' : "\033[0m";
    
    return """
	--------------------------------------------------------------------------------------
	|                                                                                    |     
	| ${c_blueish}     _   _ ______   ${c_reset}         ${c_greenish}_____  _____ _____${c_reset}                                    |
	| ${c_blueish}    | \\ | |  ____| ${c_reset}         ${c_greenish}|  __ \\|  __ \\_   _|${c_reset}    ${c_blueish}-. ${c_greenish}.-.   ${c_blueish}.-. ${c_greenish}.-.   ${c_blueish}.-. ${c_greenish}.-.   ${c_blueish}.${c_reset} |
	| ${c_blueish}    |  \\| | |__     ${c_reset}${c_greenish}______  ${c_greenish}| |__) | |  | || |${c_reset}        ${c_blueish}\\   ${c_greenish}\\ ${c_blueish}/   ${c_blueish}\\   ${c_greenish}\\ ${c_blueish}/   ${c_blueish}\\   ${c_greenish}\\ ${c_blueish}/${c_reset}  |
	| ${c_blueish}    | . ` |  __|   ${c_reset}${c_greenish}|______| ${c_greenish}|  ___/| |  | || |${c_reset}       ${c_greenish}/ ${c_blueish}\\   ${c_greenish}\\   / ${c_blueish}\\   ${c_greenish}\\   / ${c_blueish}\\   ${c_greenish}\\${c_reset}   |
	| ${c_blueish}    | |\\  | |     ${c_reset}          ${c_greenish}| |    | |__| || |_${c_reset}     ${c_greenish}~   ${c_blueish}`-~ ${c_greenish}`-`   ${c_blueish}`-~ ${c_greenish}`-`   ${c_blueish}`-~ ${c_greenish}`-${c_reset} | 
	| ${c_blueish}    |_| \\_|_|      ${c_reset}         ${c_greenish}|_|    |_____/_____|${c_reset}                                   |
	|                                                                                    |              
	| ${c_purpleish}    nf-PDI/data processing v${workflow.manifest.version}${c_reset}                                                      |
	${c_dim}--------------------------------------------------------------------------------------${c_reset}
	     01001110 01000110  00101101  01010000 01000100 01001001
	     
      P R O T E O G E N O M I C S  DATA INTEGRATION
	     ==============================================
	     Author           : $workflow.manifest.author
	     Nextflow version : $nextflow.version
	     Pipeline version : v$workflow.manifest.version
	     Description      : $workflow.manifest.description
	     ==============================================
	     Genome	      : ${params.genome}
	     annotation       : ${params.annotation}
	     ============================================== 
	
	""".stripIndent()
}
 
/* 
 * completion handler
 */
workflow.onComplete {
	log.info ( workflow.success ? "\nDone! Workflow runName: $workflow.runName has ran succesfully " : "Oops .. something went wrong" )
}
 
