#!/usr/bin/env nextflow

/*
 * Copyright (c) 2020, European Research Institute
 * for the Biology of Ageing(ERIBA)<http://eriba.umcg.nl/>
 * Department of the aging biologyha
*/

/*
 * A pipeline implemented in nextflow that serves to prepare the user
 * for running the proteogenomics data integration pipeline. It does so
 * by automatically downloading a genome and annotation file and by building an
 * genome inex.
 *
 * Authors:
 * - Stijn Arends <stijnarends@live.nl>
 *
 * Version:
 * - v1.0
 *
 * Date:
 * - 9-10-2020
*/

//  Enable dsl 2
nextflow.enable.dsl = 2

// Assign a begin value to avoid warning message
params.help            = false
params.resume          = false
params.monochrome_logs = false


def helpMessage() {
	log.info nfcoreHeader()
	log.info """

	Usage:
		Typical command for running the pipeline goas as follows:

		nextflow run reference_prepping.nf --reference </dir/to/store/files> --species <species_name> --release <101> --sequence_length <readlength>

	Description:
		Version 1.0

		This pipelines main goal is to prepare data for the main pipeline, which is
		the proteogenomic data integration pipeline. Species name and release have to be
		specified in order for the pipeline to download the right genome and annotation file
		which will be used to build an genome index with STAR. Also a directory needs to
		defined which will store the downloaded genome and annotation file as well as the
		index that has been build. These files will later be used in the ain pipeline.

	Options:
		--help
			Display the help message to the screen
			
		--monochrome_logs 
			Disable the colors of the banner
		
		Sequence read information:
			--sequence_length <integer>
				Length of a sequence read. Will be used to calculate the --sjdbOverhang 
				parameter of STAR
				
			--end <string>
				Either select PE for paired end reads or SE for single end reads

			--seq_type <string>
				Sequence type, either select DNA or RNA
		
		Genome info:
			--species <string>
				Name of the species that will be used to download the the reference files
				file
				
			--release <string>
				Specify which Ensembl API release will be used to download the the reference files

			--genome_buiild <string>
				Specify genome build that will be used to download the reference files
			
		Output:
			--outdir <string>
				Specify the location where the results will be generated 
		
			--reference <string>
				Specify the location where the files will be downloaded en the index will be
				build
		
		Data:
			--data_dir <string>
				Specify the location of the input files

		Filters:
			--vcfQual <integer>
				Quality cutoff value for processing the VCF files.

			--fpkm <string>
				FPKM filter

""".stripIndent()
}

if (params.help) exit 0, helpMessage()

log.info nfcoreHeader()
sjdbOverhang = params.sequence_length.toInteger() - 1 // Calculate the overhang that will be used in the buildIndex and STAR processes

/*---------*
* WORKFLOWS
*----------*/

/*
 The main workflow.
 Call the PREPAREREFS workflow
*/

workflow {
	PREPAREREFS()
}

/*
 The PREPAREREFS workflow prepares the user to run the 'main.nf' pipeline.
 It downloads multiple files and a VEP cache, builts an index and creats a database for blastp.
*/
workflow PREPAREREFS {
	main:
		getDownloadPath(params.references, params.species, params.genome_build, params.release)
		reference_info = getDownloadPath.out.reference_info

		downloadCDNA( reference_info )

		downloadPfammDatabase(reference_info)

		downloadPeptide( reference_info )
		makeBlastpDatabase(reference_info, downloadPeptide.out.peptide)

		downloadAnnotation( reference_info )
		annotation = downloadAnnotation.out.annotation

		// Download the necessary files from ensemble
		downloadGenome( reference_info )
		genome = downloadGenome.out.genome
		createFastaDict( genome, reference_info )
		createFastaIndex( genome, reference_info )
		buildIndex( genome, annotation, sjdbOverhang, reference_info )

		downloadVEPCache( reference_info )
		vep_related_paths = downloadVEPCache.out.vep_paths
		//createFakeVCF(genome, annotation, vep_related_paths, reference_info )
		//initializeHaplosaurusCache(genome, annotation, vep_related_paths, createFakeVCF.out.vep_index_spoof, reference_info )
}


/*---------*
* PROCESSES
*----------*/

process getDownloadPath {
	input:
		path reference_dir
		val species
		val genome_build
		val release

	output:
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(output_path), emit: "reference_info"

	script:
		output_path = "${reference_dir}/${species}/${genome_build}/${release}/"
		capitalized_species_name = species.capitalize() // get the species name with first letter in upper case
		"""
		## make sure that the output directory exists
		mkdir -p "${output_path}/";
		"""
}

/*
 Process downloadVEPCache downloads and un-tars a premade, and potentially indexed, VEP cache from ensemble.
 It uses the species name, genome build and the Ensemble API release specified in the configuration file to resolve the link

 Note:
 indexed VEP caches are only available for Ensembl API release >= 92

 input:
	reference_dir - path
		output path

 Output:
	genome - path
		A genome file in .fa format
*/

process downloadVEPCache {
	input:
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		tuple path(output_path), path(full_output_path), emit: 'vep_paths'

	script:
		output_path = "${reference_dir}/VEP/"
		full_output_path = "${output_path}/${species}/${release}_${genome_build}/"

		// indexed VEP caches are only available for Ensembl API release >= 92 !!
		index_type = release.toInteger() >= 92 ? "indexed_vep_cache" : "vep"
		if (release.toInteger() < 92) {
			println "Make sure to implement VEP cache indexing for Ensembl releases pre-dating version 92 !!"
		}

		url = "ftp://ftp.ensembl.org/pub/release-${release}/variation/${index_type}/${species}_vep_${release}_${genome_build}.tar.gz"
		"""
		## make sure that the output directory exists
		rm --force -r "${output_path}/";
		mkdir -p "${output_path}/";
		## download the .tar.gz file and stream it to tar to uncrompress it in the output directory
		wget -O - "${url}" | tar -xzvf - -C "${output_path}/";
		"""
}

/*
 Process downloadGenome downloads and unzips a genome file from ensemble.
 It uses the species name, Ensemble API release and genome build specified in the configuration file
 to determine which file it needs to download.

 input:
	reference_dir - path
		output path

 Output:
	genome - path
		A genome file in .fa format
*/
process downloadGenome {
	input:
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_file, emit: 'genome'

	script:
	output_path = "${reference_dir}/genome/"
	output_file = "${output_path}/${species}_genome.fa"
	// This filename pattern doesn't allway exist.
	// But while the other filename pattern seems to exist for many more species it causes problem with indexing the genome with star for human
	// Being able to contscuct a genome index for mapping >> handling more species
	url = "http://ftp.ensembl.org/pub/release-${release}/fasta/${species}/dna/${capitalized_species_name}.${genome_build}.dna.primary_assembly.fa.gz"
	//url = "http://ftp.ensembl.org/pub/release-${params.release}/fasta/${params.species}/dna/${params.capitalized_species_name}.${params.genome_build}.dna_sm.toplevel.fa.gz"
	"""
	mkdir -p "${output_path}";
	## download the .fa.gz file and save the uncompressed stream to the output file
	wget -O - "${url}" | gunzip -c 1> "${output_file}";
	"""
}

/*
 Process downloadAnnotation downloads and unzips a annotaion file from ensemble.
 It uses the species name, Ensemble API release and genome build specified in the configuration file
 to determine which file it needs to download.

 input:
	reference_dir - path
		output path

 Output:
	annotation - path
		A annotation file in .gtf format
*/
process downloadAnnotation {
	input:
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_file, emit: 'annotation'

	script:
		output_path = "${reference_dir}//genome/"
		output_file = "${output_path}/${species}_annotation.gtf"
		url = "http://ftp.ensembl.org/pub/release-${release}/gtf/${species}/${capitalized_species_name}.${genome_build}.${release}.gtf.gz"
		"""
		mkdir -p "${output_path}";
		## download the .gft.gz file and save the uncompressed stream to the output file
		wget -O - "${url}" | gunzip -c 1> "${output_file}";
		"""
}

/*
 Process downloadPeptide downloads and unzips a peptide file from ensemble.
 It uses the species name, Ensemble API release and genome build specified in the configuration file
 to determine which file it needs to download.

 input:
	reference_dir - path
		output path

 Output:
	peptide - path
		A peptide reference file in .fa format
*/
process downloadPeptide {
	input:
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_file, emit: 'peptide'

	script:
		output_path = "${reference_dir}/genome/"
		output_file = "${output_path}/${species}_pep.fa"
		url = "http://ftp.ensembl.org/pub/release-${release}/fasta/${species}/pep/${capitalized_species_name}.${genome_build}.pep.all.fa.gz"
		"""
		mkdir -p "${output_path}";
		## download the .fa.gz file and save the uncompressed stream to the output file
		wget -O - "${url}" | gunzip -c 1> "${output_file}";
		"""
}

/*
 Process downloadcDNA downloads and unzips a cDNA file from ensemble.
 It uses the species name, Ensemble API release and genome build specified in the configuration file
 to determine which file it needs to download.

 This file holds the cDNA sequences corresponding to Ensembl gene
 predictions. cDNA consists of transcript sequences for actual and possible
 genes, including pseudogenes, NMD and the like.

 input:
	reference_dir - path
		output path

 Output:
	cDNA - path
		A cDNA file in .fa format
*/
process downloadCDNA {
	input:
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_file, emit: 'cDNA'

	script: // We need to somehow add the params.genome_build into either the path or the filename ....
		output_path = "${reference_dir}/genome/"
		output_file = "${output_path}/${species}_cDNA.fa"
		url = "http://ftp.ensembl.org/pub/release-${release}/fasta/${species}/cdna/${capitalized_species_name}.${genome_build}.cdna.all.fa.gz"
		"""
		mkdir -p "${output_path}";
		## download the .fa.gz file and save the uncompressed stream to the output file
		wget -O - "${url}" | gunzip -c 1> "${output_file}";
		"""
}

/*
 Process buildIndex takes the downloaded genome and annotation file from
 processes downloadGenome and downloadAnnotation and uses them to build an index using STAR.

 input:
	genome - path
		A genome file in .fa format
	annotation - path
		A annotation file in .gtf format
	reference_dir - path
		output path

 output:
	* - path
		All output files produced by STAR --runMode genomeGenerate
*/
process buildIndex {
	label 'star_samtool'
	cpus 12
	// maybe add a (dynamic) memory limit
	//memory { genome.size() * 15 }

	input:
		path genome
		path annotation
		val sjdbOverhang
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_path, emit: "star_genome_index_path"

	script:
		// Since the read length matters we now store it in the directory hierarchy
		output_path = "${reference_dir}/index/${sjdbOverhang}"
		"""
		echo "Genome file: ${genome}";
		echo "Annotation file: ${annotation}";
		rm --force --recursive "${output_path}";
		mkdir -p "${output_path}";
		STAR \
			--runThreadN ${task.cpus} \
			--runMode genomeGenerate \
			--genomeDir "${output_path}" \
			--genomeFastaFiles "${genome}" \
			--sjdbGTFfile "${annotation}" \
			--sjdbOverhang ${sjdbOverhang};
		"""
}

/*
 Process createDict creates a .dict file of the genome. This is necessary
 to run GATK which will be used inside the main pipeline.

 input:
	genome - path
		A genome file in .fa format
	reference_dir - path
		output path

 Output:
	dict - path
		A .dict of genome reference
*/
process createFastaDict {
	label 'gatk'

	input:
		path genome
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path genome_dict, emit:'dict'

	script:
		genome_with_path = "${reference_dir}/genome/${genome}"
		genome_dict="${genome_with_path}".take("${genome_with_path}".lastIndexOf('.')) + ".dict" // replace the .fa extension with .dict
		"""
		## need to remove existing genome dict in case it exists, as gatk refuses to overwrite it and quits with an error
		rm --force "${genome_dict}"; ### no error for tryinng to remove non-existent files
		gatk CreateSequenceDictionary \
			--REFERENCE "${genome}" \
			--SPECIES "${species}" \
			--GENOME_ASSEMBLY "${genome_build}" \
			--OUTPUT "${genome_dict}";
		"""
}

/*
 Process createFastaIndex creates a index for the genome reference in .fai format
 using samtools. This is necessary to run GATK which will be used inside
 the main pipeline.

 input:
	genome - path
		A genome reference file in .fa format
	reference_dir - path
		output path

 Output:
	index - path
		A index file in .fai format
*/
process createFastaIndex {
	label 'star_samtool'

	input:
		path genome
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_file, emit:'index'

	script:
		genome_path = "${reference_dir}/genome/${genome}"
		output_file = "${genome_path}.fai"
		"""
		samtools faidx "${genome_path}";
		"""
}
/*
 Process downloadPfammDatabase downloads and unzips a Pfamm database from EMBL-EBI.
 The Pfamm database is the compressed and indexed using the hmmpress command from the HMMER software.

 input:
	reference_dir - path
		output path

 Output:
 	pfam_database - path
 		Compressed and indexed Pfamm database
*/

process downloadPfammDatabase {
	label "hmmer"
	
	input:
	tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
	path output_path, emit: "pfam_database_path"

	script:
	output_path = "${reference_dir}/Pfam/"
	output_file = "${output_path}/Pfam-A.hmm"
	url = "http://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.gz"
	"""
	rm --force --recursive "${output_path}/";
	mkdir -p "${output_path}";
	## download the .gz file and save the uncompressed stream to the output file
	wget -O - "${url}" | gunzip -c 1> "${output_file}";

	## Compress and index the HMM database
	hmmpress "${output_file}"
	"""
}

process makeBlastpDatabase {
	label 'blast'

	input:
	tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)
	path peptide_reference

	output:
	path output_path, emit:"blast_db_path"

	script:
	output_path = "${reference_dir}/blast/database/"
	output_file = "${output_path}/${species}_db"
	"""
	echo "Container used: ${task.container}";
	mkdir -p "${output_path}";

	makeblastdb -in "${peptide_reference}" -dbtype "prot" -parse_seqids -out "${output_file}";
	"""
}

/*********************
* Not implemented yet!
**********************/

process createFakeVCF {
	input:
		path genome
		path annotation
		tuple path(VEP_dir), path(full_VEP_dir)
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		path output_file, emit: 'vep_index_spoof'

	script:
		output_file = "${full_VEP_dir}/vep_index_spoof.vcf"
		println "THE <createFakeVCF> STEP IS NOT IMPLEMENTED YET !!"
		"""
		echo "THE <createFakeVCF> STEP IS NOT IMPLEMENTED YET !!";
		#python3 SOMESSCRIPTNAMEHERE.py "${genome}" "${annotation}" "${output_file}";
		touch "${output_file}";
		"""
}


process initializeHaplosaurusCache {
	input:
		path genome
		path annotation
		tuple path(VEP_dir), path(full_VEP_dir)
		path vep_index_spoof
		tuple val(species), val(capitalized_species_name), val(genome_build), val(release), path(reference_dir)

	output:
		val true

	script:
		vcf_path = "${full_VEP_dir}/${vep_index_spoof}"
		println "THE <initializeHaplosaurusCache> STEP IS NOT IMPLEMENTED YET !!"
		"""
		echo "THE <initializeHaplosaurusCache> STEP IS NOT IMPLEMENTED YET !!";
		cat "${vcf_path}";
		sleep 3;
		"""
}


def nfcoreHeader() {
	// Log colors ANSI codes // see for example https://godoc.org/github.com/whitedevops/colors
    c_blueish   = params.monochrome_logs ? '' : "\033[38;5;21m";
    c_greenish  = params.monochrome_logs ? '' : "\033[38;5;119m";
    c_purpleish = params.monochrome_logs ? '' : "\033[38;5;93m";
    // style control characters
    c_dim       = params.monochrome_logs ? '' : "\033[2m";
		c_reset     = params.monochrome_logs ? '' : "\033[0m";
		
		//reference_info = getDownloadPath(params.references, params.species, params.genome_build, params.release)
		//reference_dir  = ref_info[4]

    return """
	--------------------------------------------------------------------------------------
	|                                                                                    |
	| ${c_blueish}     _   _ ______   ${c_reset}         ${c_greenish}_____  _____ _____${c_reset}                                    |
	| ${c_blueish}    | \\ | |  ____| ${c_reset}         ${c_greenish}|  __ \\|  __ \\_   _|${c_reset}    ${c_blueish}-. ${c_greenish}.-.   ${c_blueish}.-. ${c_greenish}.-.   ${c_blueish}.-. ${c_greenish}.-.   ${c_blueish}.${c_reset} |
	| ${c_blueish}    |  \\| | |__     ${c_reset}${c_greenish}______  ${c_greenish}| |__) | |  | || |${c_reset}        ${c_blueish}\\   ${c_greenish}\\ ${c_blueish}/   ${c_blueish}\\   ${c_greenish}\\ ${c_blueish}/   ${c_blueish}\\   ${c_greenish}\\ ${c_blueish}/${c_reset}  |
	| ${c_blueish}    | . ` |  __|   ${c_reset}${c_greenish}|______| ${c_greenish}|  ___/| |  | || |${c_reset}       ${c_greenish}/ ${c_blueish}\\   ${c_greenish}\\   / ${c_blueish}\\   ${c_greenish}\\   / ${c_blueish}\\   ${c_greenish}\\${c_reset}   |
	| ${c_blueish}    | |\\  | |     ${c_reset}          ${c_greenish}| |    | |__| || |_${c_reset}     ${c_greenish}~   ${c_blueish}`-~ ${c_greenish}`-`   ${c_blueish}`-~ ${c_greenish}`-`   ${c_blueish}`-~ ${c_greenish}`-${c_reset} |
	| ${c_blueish}    |_| \\_|_|      ${c_reset}         ${c_greenish}|_|    |_____/_____|${c_reset}                                   |
	|                                                                                    |
	| ${c_purpleish}    nf-PDI/reference prepping v${workflow.manifest.version}${c_reset}                                                   |
	${c_dim}--------------------------------------------------------------------------------------${c_reset}
	     01001110 01000110  00101101  01010000 01000100 01001001

      P R O T E O G E N O M I C S  DATA INTEGRATION
	     ==============================================
	     Author           : ${workflow.manifest.author}
	     Nextflow version : ${nextflow.version}
	     Pipeline version : v${workflow.manifest.version}
	     Description      : ${workflow.manifest.description}
	     ==============================================
	     Species       : ${params.species}
	     Genome build  : ${params.genome_build}
	     Ensembl-API   : v${params.release}
	     ==============================================

	""".stripIndent()
}

/* completion handler */
workflow.onComplete {
	log.info ( workflow.success ? "\nDone!\nWorkflow runName: $workflow.runName ran succesfully.\nThe references should now be ready for use by the remainder of the pipeline.\n" : "Oops .. something went wrong.\nFailed to preparing the references needed for the next steps.\nIf this is the first time this happens try re-running the pipeline again whilst adding the '-resume' flag.\n" )
}
