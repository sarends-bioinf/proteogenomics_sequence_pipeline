# Proteogenomics Data Inegration: Output
* * *

This document describes the output files produced by the [reference prepping](../reference_prepping.nf) and [main](../main.nf) pipelines.

## Reference prepping pipeline overview

The reference prepping pipeline is built with [Nextflow](https://www.nextflow.io/) and contains the following steps:

* [downLoadGenome](#markdown-header-downloadgenome)
* [downLoadAnnotation](#markdown-header-downloadannotation)
* [downLoadPeptide](#markdown-header-downloadpeptide)
* [downLoadCDNA](#markdown-header-downloadcdna)
* [downLoadVEPCache](#markdown-header-downloadvepcache)
* [buildIndex](#markdown-header-downloadbuildindex)
* [createFastaDict](#markdown-header-createfastadict)
* [createFastaIndex](#markdown-header-createfastaindex)
* [downLoadPfammDatabase](#markdown-header-downloadpfammdatabase)
* [makeBlastpDatabase](#markdown-header-makeblastpdatbase)

> The output directory of these steps are based on species name, Ensembl API release and genome build.
> As an example, homo sapiens, 102 and GRch38 will be used for species name, Ensembl API release and genome build respectively. 

### downLoadGenome
* * *

Source: [ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/dna/](ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/dna/)

Output directory: `/references/homo_sapiens/GRch38/102/genome/`

* `<species_name>_genome.fa`
	* A genome reference file.
	
### downLoadAnnotation
* * *

Source: [ftp://ftp.ensembl.org/pub/release-102/gtf/homo_sapiens/](ftp://ftp.ensembl.org/pub/release-102/gtf/homo_sapiens/)

Output directory: `/references/homo_sapiens/genome_build/ensemble_release/genome/`

* `<species_name>_annotation.gtf`
	* Gene annotation file


### downLoadPeptide
* * *

Source: [ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/pep/](ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/pep/)

Output directory: `/references/homo_sapiens/GRch38/102/genome/`

* `<species_name>_peptide.fa`
	* Peptide reference file

### downLoadCDNA
* * *

Source: [ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/cdna/](ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/cdna/)

Output directory: `/references/homo_sapiens/GRch38/102/genome/`

* `<species_name>_cDNA.fa`
	* cDNA reference file

### downLoadVEPCache
* * *

Source: [ftp://ftp.ensembl.org/pub/release-102/variation/indexed_vep_cache/](ftp://ftp.ensembl.org/pub/release-102/variation/indexed_vep_cache/)

Output directory: `/references/homo_sapiens/GRch38/102/VEP/`

* `VEP/`
	* cache containing all transcript models, regulatory features and variant data for a species.

### buildIndex
* * *

Output directory: `/references/homo_sapiens/GRch38/102/index/`

* `index/`
	* A genome index build with STAR

### createFastaDict
* * *
Output directory: `/references/homo_sapiens/GRch38/102/genome/`

* `<species_name>_genome.fa.dict`
	* A sequence dictionary for the reference sequence
	
### createFastaIndex
* * *

Output directory: `/references/homo_sapiens/GRch38/102/genome/`

* `<species_name>_genome.fa.fai`
	* Indexed genome reference

### downLoadPfammDatabase
* * *

Source: [ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release](ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release)

Output directory: `/references/homo_sapiens/GRch38/102/Pfamm/`

* Pfamm-A.hmm
	* Compressed and indexed Pfamm databas

### makeBlastpDatabase
* * *

Output directory: `/references/homo_sapiens/GRch38/102/blast/database/`

* `<species_name>_db*`
	* A peptide database made for blast


## Main pipeline overview
* * *

The main pipeline is also built in [Nextflow](https://www.nextflow.io/) and processes data in the following steps:

* [FastQC](#markdown-header-fastqc)
* [Trimmomatic](#markdown-header-trimmomatic)
* [STAR](#markdown-header-star)
* [GATK](#markdown-header-gatk)
* [FilterVCF](#markdown-header-filtervcf)
* [VEP](#markdown-header-vep)
* [ProcessVEP](#markdown-header-processvep)
* [StringTie](#markdown-header-stringtie)
* [ProcessStringTie](#markdown-header-processstringtie)
* [GFFReadNovelSeqs](#markdown-header-gffreadnovelseqs)
* [GFFReadExonSeqs](#markdown-header-ggfreadexonseqs)
* [SpliceJunctions](#markdown-header-splicejunctions)
* [GetDiscordant](#markdown-header-getdiscordant)
* [MergeSuffixLeft](#markdown-header-mergesuffixleft)
* [MergeSuffixRight](#markdown-header-mergesuffixright)
* [Trinity](#markdown-header-trinity)
* [RenameTrinityFiles](#markdown-header-renametrinityfiles)
* [ConcatFasta](#markdown-header-concatfasta)
* [TransdecoderLongestOrfs](#markdown-header-transdcoderlongestorfs)
* [Blastp](#markdown-header-blastp)
* [Pfam](#markdown-header-pfam)
* [TransdecoderPredict](#markdown-header-transdecoderpredict)

### FastQC
* * *

[FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) gives general quality metrics about your reads. It provides information about the quality score distribution across your reads, 
the per base sequence content (%T/A/G/C). You get information about adapter contamination and other overrepresented sequences

Output directory: `proteogenomics_pipeline/<sample>/fastQC_<PE/SE>/`

* `<sample>_fastqc.html`
	* FastQC report, containing quality metrics for your untrimmed raw fastq files
* `<sample>_fastqc.zip`
	* zip file containing the FastQC report, tab-delimited data file and plot images

### Trimmomatic
* * *

Output `paired end` reads:

Output directory: `proteogenomics_pipeline/<sample>/trimmomaticPE`

* `<sample>_<forward/reverse>_paired.fastq.gz`
	* Both reads survived the processing

* `<sample>_<forward/reverese>_unpaired.fastq.gz`
	* One read pair survived the other did not
	
* `<sample>.trimlog`
	* log of all read trimmings, indicating the following details:
		* the read name
		* the surviving sequence length
		* the location of the first surviving base, aka. the amount trimmed from the start
		* the location of the last surviving base in the original read
		* the amount trimmed from the end
	
Output `single end` reads:

Output directory: `proteogenomics_pipeline/<sample>/trimmomaticSE`

* `<sample>_trimmed.fastq.gz`
	* The reads who survived the processing

* `<sample>.trimlog`
	* log of all read trimmings, indicating the following details:
		* the read name
		* the surviving sequence length
		* the location of the first surviving base, aka. the amount trimmed from the start
		* the location of the last surviving base in the original read
		* the amount trimmed from the end

### GATK
* * *

Output directory: `proteogenomics_pipeline/<sample>/gatk`

* `<sample>_gatk.vcf.gz`
	* Contains SNP, indel, and structural variation calls.
	
> Info about the [variant call format(VCF)](https://gatk.broadinstitute.org/hc/en-us/articles/360035531692-VCF-Variant-Call-Format)

### FilterVCF

Output directory: `proteogenomics_pipeline/<sample>/gatk/`

* `<sample>__gatk_filtered.vcf`
	* Contains only the variant calls that were above the quality threshold


### VEP
* * *

Output directory: `proteogenomics_pipeline/<sample>/vep`

* `<sample>_vep.txt`
	* Contains the annotated variants
	
> Info about the [default vep output](https://m.ensembl.org/info/docs/tools/vep/vep_formats.html#defaultout)

### ProcessVEP
* * *

Output directory: `proteogenomics_pipeline/<sample>/vep/`

* `<sample>_varNuclSeq.fa`
	* Filtered nucleotide sequences
* `<sample>_varPeplSeq.fa`
	* Filtered peptide sequence
	
### StringTie
* * *

Output directory: `proteogenomics_pipeline/<sample>/stringTie/`

* `<sample>__stringTie.gtf`
	* GTF file containing assembled transcripts
	
> Info about the [StringTie output](http://ccb.jhu.edu/software/stringtie/index.shtml?t=manual#output)

### ProcessStingTieOutput
* * *

Output directory: `proteogenomics_pipeline/<sample>/stringTie/`

* `<sample>_stringTie_known.fa`
	* Known transcript annotations
* `<sample>_stringTie_novel.gtf`
	* Novel transcript annotations
* `<sample>_exons.gtf`
	* Exon annotation
* `<sample>_fpkm.json`
	* Fragments Per Kilobase of transcript per Million mapped reads(FPKM) values

### GFFReadNovelSeqs
* * *

Output directory: `proteogenomics_pipeline/<sample>/gffread/`

* `<sample>__novel_gffRead.fa`
	* Novel transcript annotations in .fa format

### GFFReadExonSeqs
* * *

Output directory: `proteogenomics_pipeline/<sample>/gffread/`

* `<sample>__novel_gffRead.fa`
	* Exon annotations in .fa format

### SpliceJunctions
* * *

Output directory: `proteogenomics_pipeline/<sample>/spliceJunctions/`

* `<sample>_splicedExons.fa`
	* Included alternative splice sites of annotated transcripts

### GetDiscordant
* * *

Output directory: `proteogenomics_pipeline/<sample>/discordant`

* `<sample>_discordant.sam`
	* Contains discordant reads

### MergeSuffixLeft
* * *

Output directory: `proteogenomics_pipeline/<sample>/trinity/`

* `<sample>_leftTrinIn.fq.suffixed.fq`
	* Added /1 suffix to input fastq file

### MergeSuffixRight
* * *

Output directory: `proteogenomics_pipeline/<sample>/trinity/`

* `<sample>_rghtTrinIn.fq.suffixed.fq`
	* Added /2 suffix to input fastq file
	
	
### Trinity
* * *

Output directory: `proteogenomics_pipeline/<sample>/trinity/<sample>_trinitys/`

* `<sample>_trinity/Trinity.fasta`
	* the reconstructed transcripts 

### RenameTrinityFiles
* * *

Output directory: `proteogenomics_pipeline/<sample>/trinity/`

* `<sample>_trinity.fasta`
	* Renamed trinity output file to include sample ID

### ConcatFasta

Output directory: `proteogenomics_pipeline/<sample>/transdecoder/`

* `<sample>_merged.fa`
	* Merged fasta file containing the nucleotide sequences produced from the processVEPOutput.py script, the novel annotations produced by GFFRead and the assembled transcripts produced by trinity.  
	
### TransdecoderLongestOrfs
* * *

Output directory: `proteogenomics_pipeline/<sample>/transdecoder/<sample>_merged.fa.transdecoder_dir/`

* `<sample>_merged.fa.transdecoder_dir/longest_orfs.pep`
	* Identified ORFs that are at least 100 amino acids long

### Blastp
* * *

Output directory: `proteogenomics_pipeline/<sample>/transdecoder/<sample>_merged.fa.transdecoder_dir/`

* `<sample>_merged.fa.transdecoder_dir/blast.outfmt6`
	* Result from searching peptide sequence database with blastp

### Pfam
* * *

Output directory: `proteogenomics_pipeline/<sample>/transdecoder/<sample>_merged.fa.transdecoder_dir/`

* `<sample>_merged.fa.transdecoder_dir/pfam.domtblout`
	* Result from searching Pfam database with hmmscan

### TransdecoderPredict
* * *

Output directory: `proteogenomics_pipeline/<sample>/transdecoder/`

* `<sample_>_merged.fa.transdecoder.pep`
	* Predicted likely coding regions
