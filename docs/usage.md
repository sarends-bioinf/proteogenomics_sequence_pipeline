# Proteogenomics Data integration: usage
* * *

## Table of contents
* * *

- [Introduction](#markdown-header-introduction)
- [Running the pipeline](#markdown-header-running-the-pipeline)
	- [Produced files and directories](#markdown-header-produced-files-and-directories)
- [Configuration](#markdown-header-configuration)
	- [Configuration scopes](#markdown-header-configuration-scopes)
		- [Manifest](#markdown-header-manifest)
		- [Trace](#markdown-header-trace)
		- [Timeline](#markdown-header-timeline)
		- [Report](#markdown-header-report)
		- [Process](#markdown-header-process)
		- [Docker](#markdown-header-docker)
		- [Params](#markdown-header-params)
- [Arguments](#markdown-header-arguments)
	- [`--data_dir`](#markdown-header--data_dir)
	- [`--outdir`](#markdown-header--outdir)
	- [`--references`](#markdown-header--references)
	- [`--species`](#markdown-header--species)
	- [`--release`](#markdown-header--release)
	- [`--sequence_length`](#markdown-header--sequence_length)
	- [`--end`](#markdown-header--end)
	- [`--seq_type`](#markdown-header--seq_type)
	- [`--vcfQual`](#markdown-header--vcfqual)
	- [`--fpkm`](#markdown-header--fpkm)
	- [`--vepCacheDir`](#markdown-header--vepcachedir)
- [Arguments of the reference prepping pipeline](#markdown-header-arguments-of-the-reference-prepping-pipeline)
	- [`--genome`](#markdown-header--genome)
	- [`--annotation`](#markdown-header--annotation)
	- [`--genomeIndex`](#markdown-header--genomeindex)
	- [`--pepfile`](#markdown-header--pepfile)
	- [`--cDNA`](#markdown-header--cdna)
	- [`--genome_dict`](#markdown-header--genome_dict)
	- [`--genome_fasta_index`](#markdown-header--genome_fasta_index)
	- [`--blast_db`](#markdown-header--blast_db)
	- [`--pfam_db`](#markdown-header--pfam_db)

## Introduction
* * *

This workflow consist of two different pipelines. The first pipeline, [reference_prepping.nf](../reference_prepping.nf), prepares the user to run the main pipeline
by downloading the necessary files, and databases and by building a genome index. The pipelines both share one [nextflow.config](../nextflow.config) file so that the 
main pipeline knows where to find the necessary files and databases which will be used by the processes inside the pipeline. The main pipeline, [main.nf](../main.nf), create a comprehensive protein sequence FASTA database.   

The following modules are used in the main.nf pipeline: 

* [preprocessing.nf](../modules/preprocessing_QC.nf): Used for preprocessing the input data.
* [mapping.nf](../modules/mapping.nf): Maps the reads to a reference genome and indexes the mapped reads.
* [protein_db.nf](../modules/protein_DB.nf): Variant calling, de novo assembly and transcript assembly to create a custom peptide sequence database.

The pipeline can be run with either single end(SE) or paired end(PE) reads. This needs to be specified in the [nextflow.config](../nextflow.config) file inside the 
params scope under the name 'end'.

> The pipeline automatically creates a **execution report**, **trace report** and a **timeline report**. A execution report is a single document which includes many useful metrics about a workflow execution.
> A trace report contains some useful information about each process executed in the pipeline script, including: submission time, start time, completion time, cpu and memory used.
> A timeline report contains the timeline for all processes executed in the pipeline. To turn the automatic creation of these reports **off** go to the [nextflow.config](../nextflow.config) 
> file and under the **report**, **trace** and **timeline** scope set `enabled = false`.

## Running the pipeline
* * *

The typical commands for running the two pipelines are as follows: 

reference prepping pipeline:

```bash
nextflow run reference_prepping.nf
```

main pipeline:

```bash
nextflow run main.nf -process.echo -resume
```
> The `-resume` command line option allow the continuation of a pipeline execution since the last step that was successfully completed.
> By selecting the `-process.echo` the echo directive of all processes are set to true.

This will launch both the pipelines with the parameters and the docker images specified in the [nextflow.config](../nextflow.config) file. 
A docker image is specified for each process in the pipeline, making it very portable and easy to use.

### Produced files and directories
* * *

Note that the pipeline will create the following files/directories in your working directory:

```bash
work			# Directory containing syms links to nextflow working files
.nextflow.log	# Log file from nextflow
# Other nextflow hidden files, eg. history of pipeline runs and old logs.
```

The result for each sample will be stored on the system. The location of the results depends on the which output directory is specified by the user.
The output directory can be specified in the `params scope` inside the [nextflow.config](../nextflow.config) file by the `outdir` parameter.

For example if two samples IDs are specified: ERR1805188 and SUDO411728, the following directories are produced:

```bash
/path/to/outdir/proteogenomics_pipeline/ERR1805188/ # Each sample gets its own directory where each result for that sample will be produced
/path/to/outdir/proteogenomics_pipeline/SUDO411728/ # Each sample gets its own directory where each result for that sample will be produced
```

The results of all the processes will be stored inside the following directories:

```bash
fastQC_SE			# Contains the files produced by fastqc if the reads are single end(SE)
fastQC_PE			# Contains the files produced by fastqc if the reads are paired end(PE)
trimmomaticSE		# Contains the files produced by trimmomatic SE(single end reads)
trimmomaticPE	    # Contains the files produced by trimmomatic PE(paired end reads)
StarSE         	 	# Contains the files produced by STAR  with single end reads
StarPE				# Contains the files produced by STAR with paired end reads
gatk				# Contains the files produced by GATK and python script qualityFilterVcf
vep					# Contains the files produced by vep and the python script processVEPOutput
haplosaurus			# Contains the files produced by haplosaurus and the python script processPhasedVariants
stringTie			# Contains the files produced by stringtie and the python script processStringTieOutput
discordant			# Contains the files produced by the getDiscordant process
gffread				# Contains the files produced by the gffread processes
splicing			# Contains the files produced by the python script spliceJunctions
trinity				# Contains the files produced by trinity
transdecoder		# Contains the files produced by transdecoder, blastp and hmmscan
```

These drectories can be found inside the sample ID directories. 

## Configuration
* * * 

To run these pipelines, a configuration file named `nextflow.config` needs to be present inside the working directory where also the [reference_prepping.nf](../reference_prepping.nf) 
and [main.nf](../main.nf) files are located. An example [config](example_nextflow.config) file is included in the docs directory(as well as the actual [nextflow.config](../nextflow.config)
file which was used during the creation of the workflow) and can be used as a template.

> **Note: the indentation and the naming of the scope and paramters should not be changed, this is important for the workflow to be able to run**

### Configuration scopes
* * *

The [nextflow.config](../nextflow.config) contains different type of [scopes](https://www.nextflow.io/docs/latest/config.html#config-scopes). 
These scopes are used to organize configuration settings. This configuration file consists of seven different scopes:

* [Manifest](https://www.nextflow.io/docs/latest/config.html#scope-manifest)
* [Trace](https://www.nextflow.io/docs/latest/config.html#scope-trace)
* [timeline](https://www.nextflow.io/docs/latest/config.html#scope-timeline)
* [report](https://www.nextflow.io/docs/latest/config.html#scope-report)
* [process](https://www.nextflow.io/docs/latest/config.html#scope-process)
* [docker](https://www.nextflow.io/docs/latest/config.html#scope-docker)
* [params](https://www.nextflow.io/docs/latest/config.html#scope-params)

#### Manifest
The manifest scope is used to define some meta-data information about the pipeline.

#### Trace
The scope `Trace` is used to control the layout of the execution trace file produced by nextflow. To turn the creation of the
execution trace off set `enabled = false`. To change the layout of the execution trace change the `field` argument. The 
available fields are listed [here](https://www.nextflow.io/docs/latest/tracing.html#trace-fields).

#### Timeline
The `timeline` scope allows you to enable/disable the processes execution timeline report generated by Nextflow.
To disable the generation of the execution timeline report set `enabled = false`.

#### Report
The report scope allows you to define configuration setting of the workflow [Execution report](https://www.nextflow.io/docs/latest/tracing.html#execution-report).
To disable the generation of the execution report set `enabled = false`.

#### Process
The `process` configuration scope allows you to provide the default configuration for the processes in your pipeline.

For this pipeline each process which uses software (e.g. [fastqc](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)) is provided with a `label`. This label is used inside the process scope
to determine which process needs which docker container and what kind of [container options](https://www.nextflow.io/docs/latest/process.html#containeroptions) they need.

An example for a process with the label `fastqc`:

```bash
	withLabel:fastqc {
		container = 'biocontainers/fastqc:v0.11.9_cv7'
		containerOptions = "-v $HOME:$HOME -u \$(id -u):\$(id -g)"
	}
```

#### Docker
The `docker` configuration scope controls how Docker containers are executed by Nextflow.

#### Params
The `params` scope allows you to define parameters that will be accessible in the pipeline script.
There are multiple parameters and options that are needed for the processes in the pipeline. These parameters will be 
described in the [arguments section](#markdown-header-arguments) below.

The `params` scope is also used to define the input files used that will be used for the pipeline. The input files are defined inside the `samples` scope
which is inside the `params` scope. The `samples` scope consist of sample IDs and the files that belong to that sample.

For example, if you have single end reads it would look something like this:

```bash
params{
	samples {
		ERR1805188 {
			RNA_seq_1 = "${data_dir}/ERR1805188_1.fastq"
		}
		SUDO411728 {
			RNA_seq_1 = "${data_dir}/SUDO411728_1.fastq"
		}
}
```

Here there are two samples: `ERR1805188` and `SUDO411728`, both these sample contain one fastq file. The sample IDs and the RNA seq fastq files
are the collected inside the [main.nf](../main.nf) script. The files and sample IDs are then distributed to the [preprocessing_QC.nf](../modules/preprocessing_QC.nf) script and are further processed.
The sample IDs are used to create unique directories for each sample, explained in the [Produced files and directories](#markdown-header-produced-files-and-directories) section.

If you have paired end reads the sample scope would look something like this:

```bash
params {
	samples {
		ERR1805188 {
			RNA_seq_1 = "${data_dir}/ERR1805188_1.fastq"
			RNA_seq_2 = "${data_dir}/ERR1805188_2.fastq"
		}
		SUDO411728 {
			RNA_seq_1 = "${data_dir}/SUDO411728_1.fastq"
			RNA_seq_2 = "${data_dir}/SUDO411728_2.fastq"
		}
	}
}
```

Here the `RNA_seq_1` contains the forward reads and the `RNA_seq_2` contains the reverse reads.

> Both these examples make use of the `${data_dir}`. This is a parameter which specifies the directory where the data is stored.

## Arguments
* * *

### `--data_dir`

Use this parameter to specify where the input data is stored inside the system. For example:

```bash
# Command line
nextflow run main.nf --data_dir "/path/to/data/"

# Config file
params {
	// Path to input files
	data_dir = "/path/to/data/"
}
```

The data_dir parameter is used inside the `samples` scope to retireve the input fles(explained in the [params](#markdown-header-params) section).

### `--outdir`

Use this parameter to specify the directory where every output will be stored. For example:

```bash
# Command line
nextflow run main.nf --outdir "/path/to/output/dir/"

# Config file
params {
	// Path to output directory
	outdir = "/path/to/output/dir/"
}
```

All the files and directories that are produced([produced files and directories](#markdown-header-produced-files-and-directories)) will be stored inside this directory.

### `--references`

This parameter is used to define the output directory of the [reference_prepping.nf](../reference_prepping.nf) pipeline. The files that are downloaded and the genome index
that are downloaded and build inside the reference_prepping.nf pipeline are stored inside this directory. 

Example:

```bash
# Command line
nextflow run reference_prepping.nf --references "/path/to/reference/files/"

# Config file
params {
	references = "/path/to/reference/files/"
}
```

> **Note**: It is not necessary to run the reference_prepping.nf pipeline. But make sure to install the files and databases required and to build a genome 
> index. Make sure to change the paths and file names of the [`--genome`](#markdown-header--genome), [`--annotation`](#markdown-header--annotation), [`--genomeIndex`](#markdown-header--genomeindex),
[`--pepfile`](#markdown-header--pepfile), [`--cDNA`](#markdown-header--cdna), [`--genome_dict`](#markdown-header--genome_dict), [`--genome_fasta_index`](#markdown-header--genome_fasta_index),
[`--blast_db`](#markdown-header--blast_db), [`--pfam_db`](#markdown-header--pfam_db) parameters.

### `--species`

Specify which organism is being analyzed. This parameter is used inside the [reference_prepping.nf](../reference_prepping.nf) pipeline to download the right files.
Make sure to that the name is lowercase and spaces are replaced by a lower case. For example `Homo Sapiens`:

```bash
# Command line
nextflow run main.nf --species "homo_sapiens"

# Config file
params {
	species = "homo_sapiens"
}
```

### `--release`

Specify which release of ensemble should be used to install the reference prepping files. This parameter is used inside the [reference_prepping.nf](../reference_prepping.nf) pipeline.

Example:

```bash
# Command line
nextflow run main.nf --release "101"

# Config file
params {
	release = "101"
}
```

### `--sequence_length`

The length of the sequences. This is used to calculate the `--sjdbOverHang` parameter of `STAR` used inside the [mapping.nf](../modules/mapping.nf) script.
Example:

```bash
# Command line
nextflow run main.nf --sequence_length 50

# Config file
params {
	sequence_length = 50
}
```

### `--end`

Specify what kind of input data you have. If the data contains `single end reads` select `SE` and if the data contains `paired end reads` select `PE`.
Example for paired end reads:

```bash
# Command line
nextflow run main.nf --end "PE"

# Config file
params {
	end = "PE"
}
```

### `--seq_type`

Specify what kind of sequence data you have. Choose between either `DNA` or `RNA`.
For example:

```bash
# Command line
nextflow run main.nf --seq_type "RNA"

# Config file
params {
	seq_type = "RNA"
}
```

### `--vcfQual`

Specify the minimum  variant  quality  for  inclusion  in  the  sequence  database(value: e.g. 100, **no quotes**).
Example:

```bash
# Command line
nextflow run main.nf --vcfQual 100

# Config file
params {
	vcfQual = 100
}
```

### `--fpkm`

Specify the minimum  FPKM  value  for  inclusion  in  sequence  database  (value: e.g.'1', **including quotes!**).

```bash
# Command line
nextflow run main.nf --fpkm "1"

# Config file
params {
	fpkm = "1"
}
```

### `--vepCacheDir`

Specify the location of a directory containing the [vep cache dir](https://m.ensembl.org/info/docs/tools/vep/script/vep_cache.html).
Example:

```bash
# Command line
nextflow run main.nf --vepCacheDir "~/.vep"

# Config file
params {
	vepCacheDir = "~/.vep"
}
```

## Arguments of the reference prepping pipeline
* * *

The [reference_prepping.nf](../reference_prepping.nf) pipeline produces various files, a genome index and two databases. These files, index and databases
are automatically generated and are given a name that is based on the [`species name`](#markdown-header--species). 

What the reference_prepping.nf pipeline produces and how to change the parameter to use your own data are explained below for each argument.

> **Note**: The following applies to all the [arguments of the reference prepping pipeline](#markdown-header-arguments-of-the-reference-prepping-pipeline): 
when running the `reference_prepping.nf` pipeline A directory structure is created based on the [`--references`](#markdown-header--references), [`--species`](#markdown-header--species) and [`--release`](#markdown-header--release) arguments 
which contains all the extra information to run the [main.nf](../main.nf) pipeline.

### `--genome`

Specify the ensemble reference genome file. 

When running the `reference_prepping.nf` pipeline the `genome` argument is defined like this:

```bash
params {
	genome = "${references}/${species}/${release}/genome/${species}_genome.fa"
}
```

If you want to use your own genome reference file you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --genome "/path/to/genome/species_genome.fa"

# Config file
params {
	genome = "/path/to/genome/species_genome.fa"
}
```

### `--annotation`

Specify the ensembl GTF file containing annotation.

When running the `reference_prepping.nf` pipeline the `annotation` argument is defined like this:

```bash
params {
	annotation = "${references}/${species}/${release}/genome/${species}_annotation.gtf"
}
```

If you want to use a different annotation file you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --annotation "/path/to/annotation/species_annotation.gtf"

# Config file
params {
	annotation = "/path/to/annotation/species_annotation.gtf"
}
```

### `--genomeIndex`

Specify the path to the genome index.

When running the `reference_prepping.nf` pipeline the `genome_index` is defined like this:

```bash
params {
	genome_index = "${references}/${species}/${release}/index/"
}
```

If you want to use a different genome index you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --genome_index "/path/to/index/"

# Config file
params {
	genome_index = "/path/to/index"
}
```

### `--pepFile`

Specify the ensembl peptide sequence reference file.

When running the `reference_prepping.nf` pipeline the `pepfile` argument is defined like this:

```bash
params {
	pepfile = "${references}/${species}/${release}/genome/${species}_pep.fa"
}
```

If you want to use a different peptide sequence reference file you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --pepfile "/path/to/pep_ref/species_pep.fa"

# Config file
params {
	pepfile = "/path/to/pep_ref/species_pep.fa"
}
```

### `--cDNA`

Specify the ensembl cDNA reference file.

When running the `reference_prepping.nf` pipeline the `cDNA` argument is defined like this:

```bash
params {
	cDNA = "${references}/${species}/${release}/genome/${species}_cDNA.fa"
}
```

If you want to use a different cDNA reference file you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --cDNA "/path/to/cDNA_ref/species_cDNA.fa"

# Config file
params {
	cDNA = "/path/to/cDNA_ref/species_cDNA.fa"
}
```

### `--genome_dict`

Specify the .dict file of the genome. Produced by [gatk CreateSequenceDictionary](https://gatk.broadinstitute.org/hc/en-us/articles/360037422891-CreateSequenceDictionary-Picard-)
and is required to be in the same directory as the [`genome`](#markdown-header--genome) to be able to run [GATK](https://gatk.broadinstitute.org/hc/en-us).

When running the `reference_prepping.nf` pipeline the `genome_dict` argument is defined like this:

```bash
params {
	genome_dict = "${references}/${species}/${release}/genome/${species}_genome.dict"
}
```

If you want to use a different genome dict you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --genome_dict "/path/to/genome/species_genome.dict"

# Config file
params {
	genome_dict = "/path/to/genome/species_genome.dict"
}
```

### `--genome_fasta_index`

Specify the .fai file of the genome. Produced by [samtools faidx](http://www.htslib.org/doc/samtools-faidx.html)
and is required to be in the same directory as the [`genome`](#markdown-header--genome) to be able to run [GATK](https://gatk.broadinstitute.org/hc/en-us).

When running the `reference_prepping.nf` pipeline the `genome_fasta_index` argument is defined like this:

```bash
params {
	genome_fasta_index = "${references}/${species}/${release}/genome/${species}_genome.fa.fai"
}
```

If you want to use a different genome fasta index you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --genome_fasta_index "/path/to/genome/species_genome.fa.fai"

# Config file
params {
	genome_fasta_index = "/path/to/genome/species_genome.fa.fai"
}
```

> **Note**: It is important that the `genome`, `genome.dict` and `genome.fa.fai` files are in the same directory in order to be able
to run [GATK](https://gatk.broadinstitute.org/hc/en-us).

### `--blast_db`

Specify the path to a BLAST database, used by BLAST-p for TransDecoder.

When running the `reference_prepping.nf` pipeline the `blast_db` argument is defined like this:

```bash
params {
	blast_db = "${references}/${species}/${release}/blast/database/"
}
```

If you want to use a different blast database you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --blast_db "/path/to/blast/database/"

# Config file
params {
	blast_db = "/path/to/blast/database/"
}
```

> **Note**: The `blast_db` parameter only specifies the path where the files that together make up the blast database
are located. **Not** the name of the blast database. It is **required** that the name of the blast database files contain the
species name followed by an underscore and 'db'. For example: homo_sapiens_db* (where * equals to the extentions of the files).

### `--pfam_db`

Specify the path to a Pfam database file, used by HMMER for TransDecoder

When running the `reference_prepping.nf` pipeline the `pfam_db` argument is defined like this:

```bash
params {
	pfam_db = "${references}/${species}/${release}/Pfam"
}
```

If you want to use a different pfam database you have to change the parameter as follows:

```bash
# Command line
nextflow run main.nf --pfam_db "/path/to/Pfam/"

# Config file
params {
	pfam_db = "/path/to/Pfam/"
}
```

> **Note**: The `pfam_db` parameter only specifies the path where the files that together make up the pfam database
are located. **Not** the name of the pfam database. It is **required** that the name of the pfam database is `Pfam-A.hmm`.
