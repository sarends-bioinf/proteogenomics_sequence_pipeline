# Proteogenomics data integration #
* * *
A proteogenomics pipeline for more comprehensive protein sequence FASTA databases, 
including isoforms derived from SAAVs, indels, ORF-altering variants, alternative splice-junctions, 
genes not annotated as coding, fusion genes, etc.

[![Nextflow](https://img.shields.io/badge/nextflow-%E2%89%A520.07.1-brightgreen.svg)](https://www.nextflow.io/)
[![Docker](https://img.shields.io/docker/automated/nfcore/rnafusion.svg)](https://hub.docker.com/u/8656)
[![Docker biocontainers](https://img.shields.io/docker/automated/biocontainers/fastqc)](https://hub.docker.com/r/biocontainers/fastqc)
[![Docker biocontainers](https://img.shields.io/docker/automated/chrishah/trimmomatic-docker)](https://hub.docker.com/r/chrishah/trimmomatic-docker)
[![Docker biocontainers](https://img.shields.io/docker/automated/broadinstitute/gatk)](https://hub.docker.com/r/broadinstitute/gatk)
[![Docker biocontainers](https://img.shields.io/docker/automated/ensemblorg/ensembl-vep)](https://hub.docker.com/r/ensemblorg/ensembl-vep)
[![Docker biocontainers](https://img.shields.io/docker/automated/trinityrnaseq/trinityrnaseq)](https://hub.docker.com/r/trinityrnaseq/trinityrnaseq)
[![Python version](https://img.shields.io/badge/python-3-blue)](https://www.python.org/download/releases/3.0/)

## Description
* * *
The pipeline is built using [Nextflow](https://www.nextflow.io), a workflow tool to run tasks across multiple compute infrastructures in 
a very portable manner. It comes with docker containers making installation trivial and results highly reproducible.

The entire workflow consist of two different pipelines. The first one consists of one nextflow script and 
is called [reference_prepping.nf](reference_prepping.nf). The second one consists of multiple nextflow scripts:
[main.nf](main.nf), [preprocessing_QC.nf](modules/preprocessing_QC.nf), [mapping.nf](modules/mapping.nf) and [protein_DB.nf](modules/protein_DB.nf).

The main purpose of the first pipeline is to prepare for the second pipeline by downloading the files needed
to run the second pipeline and to build a genome index which happends inside the reference_prepping.nf script. The
main purpose of the second pipeline is to create a comprehensive protein sequence FASTA database. 

The pipeline consist of multiple components listed [here](#markdown-header-components). The entire pipeline runs with the use of docker images.
So all these components don't have to be installed on your own system. Each proces has its own docker image specified and these image will be pulled automatically.

## Workflow
* * *

An overview of the reference prepping pipeline can be seen below:

![image](images/reference_prepping_workflow.jpg)

An overview of the main pipeline can be seen below:

![main](images/main_pipeline_workflow.jpg)

## Quick start
* * *
Some things are required in order to be able to run nextflow and can be found in the [requirements](#markdown-header-requirements) table below.
Nextflow can be used on any POSIX compatible system (Linux, OS X, etc). 
Windows system is supported through [WSL](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux).

i. Install [`nextflow`](https://nf-co.re/usage/installation)

*     Using wget:
```bash
        wget -qO- https://get.nextflow.io | bash
```
*     Or by using curl:
```bash
        curl -s https://get.nextflow.io | bash
```

ii. Install [`Docker`](https://docs.docker.com/engine/installation/)

Launch the pipelines with the following command:
```bash
nextflow run reference_prepping.nf
nextflow run main.nf
```
> Please see [usage docs](docs/usage.md) for all of the available options when running the pipeline.
 
## Documentation
* * *

The proteogenomics data integration and reference prepping pipeline come with documentation, found in the `docs/` directory.

1. [Quick installation](#markdown-header-quick-start)
2. [Running the pipeline](docs/usage.md)
3. [Output files produced](docs/output.md)


## Requirements
* * *
| Software                            		 | Version 			    |
| -----------------------------------------  | :------------------: |
| [Nextflow](https://www.nextflow.io) 		 | `20.07.1 (or later)` |
| [Java](https://www.java.com/nl/download/)  | `8 (or later)`	    |
| [Docker](https://www.docker.com/)          | `1.10 (or  later)`   |
| [Singularity](http://singularity.lbl.gov)  | `2.5.2 (or later)`   |
| [GATK](https://gatk.broadinstitute.org/)   | `4.1.x`              |
| [BASH](https://www.gnu.org/software/bash/) | `3.2`                |


## Components
* * *
The pipeline uses the following tools and software:

| Software/tool                                                                | Version       | Docker image 				          |
| ---------------------------------------------------------------------------- | :-----------: | :----------------------------------: |
| [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)         | `0.11.9`      | biocontainers/fastqc:v0.11.9_cv7     |
| [Trimmomatic](http://www.usadellab.org/cms/index.php?page=trimmomatic)       | `0.38`        | chrishah/trimmomatic-docker:0.38     |
| [STAR](https://github.com/alexdobin/STAR)                                    | `2.6.1d`      | 8656/star_samtools:1.0               |
| [Samtools](http://www.htslib.org/)                                           | `1.10`        | 8656/star_samtools:1.0               |
| [GATK](https://gatk.broadinstitute.org/hc/en-us)						       | `4.0.11.0`    | broadinstitute/gatk:4.0.11.0         |
| [VEP](https://www.ensembl.org/info/docs/tools/vep/index.html) 		       | `release-101` | ensemblorg/ensembl-vep:release_101.0 |
| [Haplosaurus](https://www.ensembl.org/info/docs/tools/vep/haplo/index.html)  | `release-101` | ensemblorg/ensembl-vep:release_101.0 |
| [StringTie](http://ccb.jhu.edu/software/stringtie/)					       | `2.1.4`       | 8656/stringtie:2.1.4                 |
| [GFFRead](http://ccb.jhu.edu/software/stringtie/gff.shtml#gffread)           | `0.10.6`      | 8656/gffread:0.10.6                  |
| [Trinity](https://github.com/trinityrnaseq/trinityrnaseq/wiki)	           | `2.11.0`	   | trinityrnaseq/trinityrnaseq:2.11.0   |
| [blastp](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE=Proteins)             | `2.9.0`       | ncbi/blast:2.9.0                     |
| [HMMER](http://hmmer.org/)                                                   | `3.2.1`       | 8656/hmmer:3.2.1                     |
| [Transdecoder](https://github.com/TransDecoder/TransDecoder/wiki)            | `5.5.0`       | 8656/transdecoder:5.5.0              |

## Credits
* * *

This pipeline was originally written by Tessa Gillit (tessagillett@hotmail.com) on behalf of the European Research Institute for
the Biology of Ageing ([ERIBA](https://eriba.umcg.nl/)). This is a follow up internship project started by BSCs bio-informatics student at the Hanze University of Applied Sciences, Stijn Arends ([stijnarends@live.nl](https://bitbucket.org/sarends-bioinf))
in order of ERIBA.

Special thanks to the people involved:

* prof. dr. Peter L. Horvatovich (p.l.horvatovich@rug.nl)
* prof. dr. Victor Guryev (victor.guryev@gmail.com)
* MSc. Yanick P. Hagemeijer (y.p.hagemeijer@rug.nl)
* MSc. student Tessa Gillit (tessagillett@hotmail.com)

## Tool references
* * * 

* ***Fastqc:*** Andrews, S. (2010). ***FastQC:  A Quality Control Tool for High Throughput Sequence Data [Online].*** Available online at: http://www.bioinformatics.babraham.ac.uk/projects/fastqc/
* ***Trimmomatic:*** Bolger, A. M., Lohse, M., & Usadel, B. (2014). ***Trimmomatic: A flexible trimmer for Illumina Sequence Data.*** Bioinformatics, btu170
* ***STAR:*** Dobin A, Davis CA, Schlesinger F, Drenkow J, Zaleski C, Jha S, Batut P, Chaisson M, Gingeras TR. ***STAR: ultrafast universal RNA-seq aligner.*** Bioinformatics. 2013 Jan 1;29(1):15-21. doi: 10.1093/bioinformatics/bts635. Epub 2012 Oct 25. PMID: 23104886; PMCID: PMC3530905.
* ***Samtools:*** Li H, Handsaker B, Wysoker A, Fennell T, Ruan J, Homer N, Marth G, Abecasis G, Durbin R; 1000 Genome Project Data Processing Subgroup. ***The Sequence Alignment/Map format and SAMtools.*** Bioinformatics. 2009 Aug 15;25(16):2078-9. doi: 10.1093/bioinformatics/btp352. Epub 2009 Jun 8. PMID: 19505943; PMCID: PMC2723002.
* ***GATK:*** ***The Genome Analysis Toolkit: a MapReduce framework for analyzing next-generation DNA sequencing data*** McKenna A, Hanna M, Banks E, Sivachenko A, Cibulskis K, Kernytsky A, Garimella K, Altshuler D, Gabriel S, Daly M, DePristo MA, 2010 GENOME RESEARCH 20:1297-303
* ***VEP:*** McLaren W, Gil L, Hunt SE, Riat HS, Ritchie GR, Thormann A, Flicek P, Cunningham F. ***The Ensembl Variant Effect Predictor.*** Genome Biology Jun 6;17(1):122. (2016) doi:10.1186/s13059-016-0974-4
* ***Haplosaurus:*** William Spooner, William McLaren, Timothy Slidel, Donna K. Finch, Robin Butler, Jamie Campbell, Laura Eghobamien, David Rider, Christine Mione Kiefer, Matthew J. Robinson, Colin Hardman, Fiona Cunningham, Tristan Vaughan, Paul Flicek & Catherine Chaillan Huntington. ***Haplosaurus computes protein haplotypes for use in precision drug design.*** Nature Communications volume 9, Article number: 4128 (2018) doi:10.1038/s41467-018-06542-1
* ***Stringtie:*** Pertea, M., Pertea, G., Antonescu, C. et al. ***StringTie enables improved reconstruction of a transcriptome from RNA-seq reads.*** Nat Biotechnol 33, 290–295 (2015). https://doi.org/10.1038/nbt.3122
* ***Gffread:*** Pertea G and Pertea M. GFF Utilities: GffRead and GffCompare [version 1; peer review: 3 approved]. F1000Research 2020, 9:304 (https://doi.org/10.12688/f1000research.23297.1)
* ***Trinity:*** Grabherr MG, Haas BJ, Yassour M, Levin JZ, Thompson DA, Amit I, Adiconis X, Fan L, Raychowdhury R, Zeng Q, Chen Z, Mauceli E, Hacohen N, Gnirke A, Rhind N, di Palma F, Birren BW, Nusbaum C, Lindblad-Toh K, Friedman N, Regev A. ***Full-length transcriptome assembly from RNA-seq data without a reference genome.*** Nat Biotechnol. 2011 May 15;29(7):644-52. doi: 10.1038/nbt.1883. PubMed PMID: 21572440.
* ***Blastp:*** Grzegorz M. Boratyn, Christiam Camacho, Peter S. Cooper, George Coulouris, Amelia Fong, Ning Ma, Thomas L. Madden, Wayne T. Matten, Scott D. McGinnis, Yuri Merezhuk, Yan Raytselis, Eric W. Sayers, Tao Tao, Jian Ye, Irena Zaretskaya, ***BLAST: a more efficient report with usability improvements, Nucleic Acids Research,*** Volume 41, Issue W1, 1 July 2013, Pages W29–W33, https://doi.org/10.1093/nar/gkt282
* ***Hmmer:*** Travis J. Wheeler, Sean R. Eddy, nhmmer: ***DNA homology search with profile HMMs***, Bioinformatics, Volume 29, Issue 19, 1 October 2013, Pages 2487–2489, https://doi.org/10.1093/bioinformatics/btt403
* ***Transdecoder:*** Brian Haas. ***TransDecoder (Find Coding Regions Within Transcripts).*** Available online at: https://github.com/TransDecoder/TransDecoder/wiki
